# Electronic lock - APO2021 semestral project

This is a repository for a semestral project of Jakub Vanek and Dmitrii Zamedianskii
for the B0B35APO course.

- Installation instructions can be found in [`docs/installing.md`](docs/installing.md).
- User guide can be found in [`docs/user_guide.md`](docs/user_guide.md).
- Configuration overview can be found in [`docs/configuration.md`](docs/configuration.md).
- Block diagram can be found in [`docs/block-diagram.png`](docs/block-diagram.png).

You can also generate a HTML documentation by installing Doxygen and running
```
make doxy
```

## TL;DR installation guide

### Building

To build the program, just run `make`. The resulting program will be named `lock.elf`.

You will need to have the `arm-linux-gnueabihf-gcc` compiler installed.
If you don't have one and you're using Ubuntu, you can install it by running:
```sh
sudo apt install crossbuild-essential-armhf
```

### Uploading & running

You can upload and run the program using dedicated Make targets. These
will run pre-specified SSH commands that simplify the deployment process.

It is expected that SSH tunnelling will be used. This means that there will
be one "tunnelling" connection to the server `postel.felk.cvut.cz`.
This connection will forward the MZ_APO SSH port to a port on your PC.

You can configure SSH parameters using the `Makefile.conf` file. You can
start by copying the `Makefile.conf.example` file and changing the applicable
variables.

Afterwards, you should be able to launch the tunnel:
```sh
make tunnel
```

Then you can run the application remotely:
```sh
make run
```
