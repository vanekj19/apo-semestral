/**
 * @file args.c
 * @brief Command line argument handler implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup args
 */

#include <stdio.h>
#include <string.h>

#include "args.h"
#include "build_config.h"
#include "config/config.h"
#include "core/logging.h"

//! print help message (-h, --help)
static void print_help(void);

//! process one argument unit
static argcmd process_arg(int *p_argi, int argc, char **argv);


argcmd args_process(int argc, char **argv)
{
    if (argc <= 1) {
        log_info("no arguments passed in, starting with default configuration.");
        return ARGS_RUN;
    }

    argcmd cmd;
    int argi = 1;
    while (argi < argc) {
        cmd = process_arg(&argi, argc, argv);
        if (cmd != ARGS_RUN) {
            break;
        }
    }

    return cmd;
}


argcmd process_arg(int *p_argi, int argc, char **argv)
{
    int argi = *p_argi;

    if (strcmp(argv[argi], "-h"    ) == 0 ||
        strcmp(argv[argi], "--help") == 0) {
        print_help();
        return ARGS_EXIT;
    }

    if (strcmp(argv[argi], "-d"    ) == 0 ||
        strcmp(argv[argi], "--debug") == 0) {
        log_enable_debug(true);
        *p_argi += 1;
        return ARGS_RUN;
    }

    if (strcmp(argv[argi], "-c"      ) == 0 ||
        strcmp(argv[argi], "--config") == 0) {
        if (argc <= argi + 1) {
            log_error("expected config file as an argument");
            return ARGS_ERROR;
        }
        const char *file = argv[argi + 1];
        if (!config_load(file, &CONFIG)) {
            log_error("cannot load config file");
            return ARGS_ERROR;
        }
        *p_argi += 2;
        return ARGS_RUN;
    }

    log_error("unknown argument: %s", argv[argi]);
    return ARGS_ERROR;
}

void print_help(void)
{
    fputs("MZ_APO electronic lock v" APP_VERSION "\n", stderr);
    fputs("Usage:\n", stderr);
    fputs("lock.elf\n", stderr);
    fputs("    -h, --help               ...  print this message\n", stderr);
    fputs("    -d, --debug              ...  enable debug output\n", stderr);
    fputs("    -c FILE, --config FILE   ...  load a configuration file\n", stderr);
}
