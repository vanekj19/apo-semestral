/**
 * @file encoders.h
 * @brief Public API of the MZ_APO encoder driver module
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_core
 */

#ifndef ENCODERS_INTERFACE_H
#define ENCODERS_INTERFACE_H

#include "build_config.h"

#include <time.h>
#include <stdbool.h>
#include <termios.h>
#include "core/mmio_regs.h"
#include "core/mmio_map.h"

/**
 * @addtogroup encoder_core
 * @{
 */

//! Knob enumeration
enum {
    KNOB_R, //!< Red knob (index 0)
    KNOB_G, //!< Green knob (index 1)
    KNOB_B, //!< Blue knob (index 2)
    KNOB_NR //!< Number of knobs
};

//! HW knob state (real or emulated)
typedef struct {
    struct timespec pressed_since[KNOB_NR]; //!< Since when a given encoder is pressed
    uint8_t event_ref[KNOB_NR];             //!< Event reference position for a given encoder
    uint8_t value[KNOB_NR];                 //!< Position of a given encoder
    bool pressed[KNOB_NR];                  //!< Whether a given encoder is pressed
} input_ll_state;

//! Decoded knob state
typedef struct {
    struct timespec lock_pressed_since; //!< Since when the unlock/lock knob is pressed.
                                        //!< Stored as timestamp from CLOCK_MONOTONIC
    int             code;          //!< Current code
    bool            lock_pressed;  //!< Whether the unlock/lock knob is pressed.
} input_hl_state;


//! Encoder module state
typedef struct {
    enc_if         *mem;     //!< Encoder peripheral hardware address
    bool           tio_rdy;  //!< Stdin termios was configured
    struct termios tio;      //!< Saved stdin termios state.

    input_ll_state stdin_ll; //!< tracked stdin state
    input_ll_state hw_ll;    //!< tracked hwstate
    input_hl_state current;  //!< current decoded state
} encoders;


/**
 * @brief Initialize the encoder controller module.
 *
 * Hardware encoders will be always initialized. Stdin-based emulation
 * will be initialized only if CONFIG.emulate_encoders is enabled.
 *
 * @param self Module state to initialize.
 * @returns Whether the initialization succeeded or not.
 */
extern bool encoder_init(encoders *self);

/**
 * @brief Deinitialize the encoder controller module.
 * @param self Module state to deinitialize.
 */
extern void encoder_deinit(encoders *self);

/**
 * @brief Update the public encoder state based on the current encoder position.
 * @param self Encoder module state.
 */
extern void encoder_update(encoders *self);

/**
 * @brief Reinitialize encoder reference offsets and reset the current code to 000.
 * @param self Pointer to the state struct.
 */
extern void encoder_reset(encoders *self);

/**@}*/
#endif // ENCODERS_INTERFACE_H
