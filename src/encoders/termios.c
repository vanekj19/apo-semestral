/**
 * @file termios.c
 * @brief Utilities for configuring TTY attributes
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_termios
 */

#include <termios.h>
#include "encoders/termios.h"
#include "core/logging.h"

//! termios attribute - no lower limit on minimum bytes to return in a single read()
#define READ_MINSIZE_NONE 0

//! termios attribute - do not wait until returning from read()
#define READ_TIMER_NONE 0

//! Switch stdin to raw mode
static bool configure_kbd(struct termios *tio);


////////////////////////////////////////////////////////////////
// Wrapper functions
//

bool termios_init_kbd(int fd, struct termios *old)
{
    if (tcgetattr(fd, old) < 0) {
        log_sys_error("cannot query terminal parameters");
        return false;
    }

    struct termios new_tio = *old;
    if (!configure_kbd(&new_tio)) {
        log_error("cannot configure termios");
        return false;
    }

    if (tcsetattr(fd, TCSANOW, &new_tio) < 0) {
        log_sys_error("cannot set terminal parameters");
        return false;
    }

    return true;
}

bool termios_restore(int fd, struct termios *old)
{
    if (tcsetattr(fd, TCSAFLUSH, old) < 0) {
        log_sys_error("cannnot restore old termios");
        return false;
    }
    return true;
}

////////////////////////////////////////////////////////////////
// Termios filling code
//

bool configure_kbd(struct termios *tio)
{
    // raw operation
    cfmakeraw(tio);
    tio->c_oflag |= OPOST;
    tio->c_lflag |= ISIG;

    // "soft" non-blocking read
    tio->c_cc[VTIME] = READ_TIMER_NONE;
    tio->c_cc[VMIN]  = READ_MINSIZE_NONE;

    return true;
}
