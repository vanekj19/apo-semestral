#include "config/config.h"

/**
 * @defgroup encoders Encoder module
 * @brief Wrapper around hardware and emulated rotary encoders/knobs
 *
 * This module provides a high-level view of the encoder input interface.
 * Tracking of the entered numeric code is implemented here, for example.
 *
 * There are two underlying implementations of the module:
 *  - Hardware-based one that uses the SPILED peripheral. This backend
 *    is always enabled.
 *  - Emulation-based one that uses raw stdin input to partially emulate
 *    the encoder hardware. This module can be enabled or disabled at
 *    start time by modifying the CONFIG.emulate_encoders member.
 */

/**
 * @defgroup encoder_core Encoder module core
 * @brief Public API & its implementation
 * @ingroup encoders
 */

/**
 * @defgroup encoder_utils State tracking utils
 * @brief Utilities for updating tracked encoder state
 * @ingroup encoders
 */


/**
 * @defgroup encoder_hw Hardware interop
 * @brief Readout of hardware encoder tracker
 * @ingroup encoders
 */

/**
 * @defgroup encoder_stdin Raw stdin interop
 * @brief Implementation of stdin-based encoder emulation
 * @ingroup encoders
 */

/**
 * @defgroup encoder_termios Termios management
 * @brief Function for switching console to raw mode
 * @ingroup encoder_stdin
 */
