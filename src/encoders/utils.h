/**
 * @file utils.h
 * @brief State-tracking utility functions header
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_utils
 */

#ifndef ENCODERS_UTILS_H
#define ENCODERS_UTILS_H

#include "encoders/encoders.h"

/**
 * @addtogroup encoder_utils
 * @{
 */

//! Reset knob state to initial state
extern void encoder_st_reset(input_ll_state *state, int knob);

//! Reset knob state to initial state
extern void encoder_st_reset_all(input_ll_state *state);

//! Reset knob state to initial state
extern void encoder_st_reset_refs(input_ll_state *state);

//! Set knob press state
extern void encoder_st_press(input_ll_state *state, int knob, bool pressed);

//! Increment knob by a tick
extern void encoder_st_value_add(input_ll_state *state, int knob, int delta);

//! Increment knob by a digit-tick
extern void encoder_st_value_add_digit(input_ll_state *state, int knob, int digits);

//! Print one encoder state into log
extern void encoder_st_print(input_ll_state *state, const char *name, int knob);

//! Print encoder state into log
extern void encoder_st_print_all(input_ll_state *state);

/**@}*/

#endif // ENCODERS_UTILS_H
