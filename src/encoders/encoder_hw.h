/**
 * @file encoder_hw.h
 * @brief Hardware access to MZ_APO encoders
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_hw
 */

#ifndef ENCODERS_ENCODER_HW_H
#define ENCODERS_ENCODER_HW_H

#include <stdint.h>
#include <stdbool.h>
#include "core/mmio_regs.h"
#include "encoders/encoders.h"

/**
 * @addtogroup encoder_hw
 * @{
 */

/**
 * @brief Initialize encoder controller & map it to program memory.
 * @param self Encoder module state.
 * @returns Whether the initialization succeeded or not.
 */
extern bool encoder_hw_init(encoders *self);

/**
 * @brief Deinitialize encoder controller & numap it from memory.
 * @param self Encoder module state.
 */
extern void encoder_hw_deinit(encoders *self);

/**
 * @brief Update lowlevel state based on HW encoder position.
 * @param self Encoder module state.
 */
extern void encoder_hw_update(encoders *self);

/**@}*/
#endif // ENCODERS_ENCODER_HW_H
