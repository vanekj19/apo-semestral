/**
 * @file encoders.c
 * @brief Implementation of common encoder digit tracking
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_core
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "build_config.h"
#include "config/config.h"
#include "encoders/encoders.h"
#include "encoders/encoder_hw.h"
#include "encoders/encoder_stdin.h"
#include "encoders/utils.h"
#include "core/logging.h"
#include "core/mmio_regs.h"
#include "core/common.h"


//! Helper macro for number of decimal digits
#define DECIMAL_DIGITS 10

//! Determine which digit does the knob currently represent.
static int decode_knob_digit(input_ll_state *ll, int knob, int old_digit);

//! Detect transition event in low-level state and transform them to high-level changes of state
static void encoder_hl_update(input_hl_state *hl, input_ll_state *ll);


////////////////////////////////////////////////////////////////
// Initialization/deinitialization
//

bool encoder_init(encoders *self)
{
    self->current.code = 0;
    self->current.lock_pressed = false;
    self->current.lock_pressed_since = now_monotonic();
    encoder_st_reset_all(&self->stdin_ll);
    encoder_st_reset_all(&self->hw_ll);

    if (!encoder_hw_init(self)) {
        return false;
    }
    if (CONFIG.emulate_encoders && !encoder_stdin_init(self)) {
        return false;
    }
    return true;
}

void encoder_deinit(encoders *self)
{
    if (self != NULL) {
        encoder_hw_deinit(self);
        if (CONFIG.emulate_encoders) {
            encoder_stdin_deinit(self);
        }
    }
}


////////////////////////////////////////////////////////////////
// Encoder state processing
//

void encoder_update(encoders *self)
{
    // hl_update only pulls this up
    self->current.lock_pressed = false;

    encoder_hw_update(self);
    encoder_hl_update(&self->current, &self->hw_ll);

    if (CONFIG.emulate_encoders) {
        encoder_stdin_update(self);
        encoder_hl_update(&self->current, &self->stdin_ll);
    }
}

void encoder_hl_update(input_hl_state *hl, input_ll_state *ll)
{
    // decode digits
    int hundreds = (hl->code / 100) % 10;
    int tens     = (hl->code /  10) % 10;
    int ones     = (hl->code /   1) % 10;
    hundreds = decode_knob_digit(ll, KNOB_R, hundreds);
    tens     = decode_knob_digit(ll, KNOB_G, tens);
    ones     = decode_knob_digit(ll, KNOB_B, ones);
    hl->code = 100*hundreds + 10*tens + 1*ones;

    // decode press
    hl->lock_pressed |= ll->pressed[KNOB_B];
    if (millis_from(&hl->lock_pressed_since) > millis_from(&ll->pressed_since[KNOB_B])) {
        hl->lock_pressed_since = ll->pressed_since[KNOB_B];
    }
}


int decode_knob_digit(input_ll_state *ll, int knob, int old_digit)
{
    uint8_t value = ll->value[knob];
    uint8_t *ref = &ll->event_ref[knob];

    int8_t delta = (int8_t)(value - *ref);
    // how many times the tick-per-digit value fits into the delta from last change
    int crossings = delta / abs(CONFIG.encoder_ticks_per_digit);

    int new_digit = old_digit;
    if (crossings != 0) {
        // add to reference position to prevent double-counting in further iterations
        *ref += crossings * abs(CONFIG.encoder_ticks_per_digit);

        int digit_change = CONFIG.encoder_ticks_per_digit > 0 ? +crossings : -crossings;

        // adjust the digit
        new_digit = (old_digit + digit_change) % DECIMAL_DIGITS;
        if (new_digit < 0) {
            // modulo result may be negative -> add offset to fix this
            new_digit += DECIMAL_DIGITS;
        }
    }
    return new_digit;
}


////////////////////////////////////////////////////////////////
// Code reset
//

void encoder_reset(encoders *self)
{
    log_info("resetting code to zero");

    encoder_st_reset_refs(&self->hw_ll);
    encoder_st_reset_refs(&self->stdin_ll);
    self->current.code = 0;
}
