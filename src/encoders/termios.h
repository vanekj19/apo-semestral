/**
 * @file termios.h
 * @brief Utilities for configuring TTY attributes
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_termios
 */

#ifndef STDIN_ENCODERS_TERMIOS_H
#define STDIN_ENCODERS_TERMIOS_H

#include <termios.h>
#include <stdbool.h>

/**
 * @addtogroup encoder_termios
 * @{
 */

/**
 * @brief Initialize kernel termios for a keyboard (Linux console)
 * @return Whether the operation succeeded or not.
 */
extern bool termios_init_kbd(int fd, struct termios *old);

/**
 * @brief Restore old kernel termios for a keyboard (Linux console).
 * @return Whether the operation succeeded or not.
 */
extern bool termios_restore(int fd, struct termios *old);

/**@}*/
#endif // STDIN_ENCODERS_TERMIOS_H
