/**
 * @file encoder_stdin.c
 * @brief stdin-backed emulation of rotary encoders
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_stdin
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include "build_config.h"

#include "encoders/encoders.h"
#include "encoders/encoder_stdin.h"
#include "encoders/utils.h"
#include "encoders/termios.h"
#include "core/logging.h"
#include "core/mmio_regs.h"
#include "core/common.h"

//! Print a help message for navigating the TUI
static void print_help(void);

//! Process a received character
static void process_char(encoders *self, char cmd);


////////////////////////////////////////////////////////////////
// Initialization/deinitialization
//

bool encoder_stdin_init(encoders *self)
{
    print_help();
    encoder_st_reset_all(&self->stdin_ll);
    self->tio_rdy = false;
    if (!termios_init_kbd(STDIN_FILENO, &self->tio)) {
        return false;
    }
    self->tio_rdy = true;
    return true;
}

void encoder_stdin_deinit(encoders *self)
{
    if (self->tio_rdy) {
        termios_restore(STDIN_FILENO, &self->tio);
        self->tio_rdy = false;
    }
}


////////////////////////////////////////////////////////////////
// Event processing
//

void encoder_stdin_update(encoders *self)
{
    char cmd = '\0';
    while (read(STDIN_FILENO, &cmd, sizeof(cmd)) > 0) {
        process_char(self, cmd);
    }
}

void print_help(void)
{
    log_warn("Fake encoders enabled.");
    log_info("Usage: ");
    log_info(" - a/z - simulate rotation of red encoder knob");
    log_info(" - s/x - simulate rotation of green encoder knob");
    log_info(" - d/c - simulate rotation of blue encoder knob");
    log_info(" - f/v - press/release the blue encoder button");
    log_info(" - i   - dump current encoder state");
    log_info(" - h   - print this message");
}

void process_char(encoders *self, char cmd)
{
    input_ll_state *st = &self->stdin_ll;
    switch (cmd) {
    case 'a':
        log_info("turning red knob by +1 digit");
        encoder_st_value_add_digit(st, KNOB_R, +1);
        break;
    case 'z':
        log_info("turning red knob by -1 digit");
        encoder_st_value_add_digit(st, KNOB_R, -1);
        break;
    case 's':
        log_info("turning green knob by +1 digit");
        encoder_st_value_add_digit(st, KNOB_G, +1);
        break;
    case 'x':
        log_info("turning green knob by -1 digit");
        encoder_st_value_add_digit(st, KNOB_G, -1);
        break;
    case 'd':
        log_info("turning blue knob by +1 digit");
        encoder_st_value_add_digit(st, KNOB_B, +1);
        break;
    case 'c':
        log_info("turning blue knob by -1 digit");
        encoder_st_value_add_digit(st, KNOB_B, -1);
        break;
    case 'f':
        log_info("pressing blue button");
        encoder_st_press(st, KNOB_B, true);
        break;
    case 'v':
        log_info("releasing blue button");
        encoder_st_press(st, KNOB_B, false);
        break;
    case 'i':
        log_info("sw encoder state:");
        encoder_st_print_all(&self->stdin_ll);
        log_info("hw encoder state:");
        encoder_st_print_all(&self->hw_ll);
        break;
    case 'h':
        print_help();
        break;
    }
}
