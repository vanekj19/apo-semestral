/**
 * @file encoder_stdin.h
 * @brief stdin-backed emulation of rotary encoders
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_stdin
 */

#ifndef STDIN_ENCODERS_INTERFACE_H
#define STDIN_ENCODERS_INTERFACE_H

#include "build_config.h"

#include <time.h>
#include <stdbool.h>
#include <termios.h>
#include "core/mmio_regs.h"
#include "core/mmio_map.h"

/**
 * @addtogroup encoder_stdin
 * @{
 */

/**
 * @brief Prepare stdin-based encoder emulation.
 * @param self Module state to initialize.
 * @returns    Whether the initialization succeeded or not.
 */
extern bool encoder_stdin_init(encoders *self);

/**
 * @brief Unmap the encoder controller hardware.
 * @param self Pointer to the state struct.
 */
extern void encoder_stdin_deinit(encoders *self);

/**
 * @brief Update the encoder state based on pending stdin input.
 * @param self Pointer to the state struct.
 */
extern void encoder_stdin_update(encoders *self);

/**@}*/
#endif // STDIN_ENCODERS_INTERFACE_H
