/**
 * @file utils.c
 * @brief State-tracking utility function implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_utils
 */

#include "encoders/utils.h"
#include "config/config.h"
#include "core/common.h"
#include "core/logging.h"

void encoder_st_reset(input_ll_state *state, int knob)
{
    state->pressed_since[knob] = now_monotonic();
    state->pressed[knob]       = false;
    state->value[knob]         = 0;
    state->event_ref[knob]     = 0;
}

void encoder_st_reset_all(input_ll_state *state)
{
    for (int i = 0; i < KNOB_NR; i++) {
        encoder_st_reset(state, i);
    }
}

void encoder_st_reset_refs(input_ll_state *state)
{
    for (int i = 0; i < KNOB_NR; i++) {
        state->event_ref[i] = state->value[i];
    }
}

void encoder_st_increment(input_ll_state *state, int knob, int digits)
{
    state->value[knob] += CONFIG.encoder_ticks_per_digit * digits;
}

void encoder_st_press(input_ll_state *state, int knob, bool pressed)
{
    if (!state->pressed[knob] && pressed) {
        state->pressed_since[knob] = now_monotonic();
    }
    state->pressed[knob] = pressed;
}

void encoder_st_value_add(input_ll_state *state, int knob, int delta)
{
    state->value[knob] += delta;
}

void encoder_st_value_add_digit(input_ll_state *state, int knob, int digits)
{
    encoder_st_value_add(state, knob, digits * CONFIG.encoder_ticks_per_digit);
}

void encoder_st_print(input_ll_state *state, const char *name, int knob)
{
    log_info("%s: pos %d, ref %d, pressed %s, pressed %ld ms ago",
        name, state->value[knob], state->event_ref[knob],
        state->pressed[knob] ? "yes" : "no",
        millis_from(&state->pressed_since[knob]));
}

void encoder_st_print_all(input_ll_state *state)
{
    encoder_st_print(state, "KNOB_R", KNOB_R);
    encoder_st_print(state, "KNOB_G", KNOB_G);
    encoder_st_print(state, "KNOB_B", KNOB_B);
}
