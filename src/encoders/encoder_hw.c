/**
 * @file encoder_hw.c
 * @brief Hardware access to MZ_APO encoders
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup encoder_hw
 */

#include "encoders/encoder_hw.h"
#include "encoders/encoders.h"
#include "encoders/utils.h"


////////////////////////////////////////////////////////////////
// Initialization/deinitialization
//

bool encoder_hw_init(encoders *self)
{
    self->mem = mmio_map(SPILED_HWADDR, SPILED_HWSIZE, false);
    bool ok = self->mem != NULL;

    if (ok) {
        encoder_st_reset_all(&self->hw_ll);
        encoder_hw_update(self);
        encoder_st_reset_refs(&self->hw_ll);
    }

    return ok;
}

void encoder_hw_deinit(encoders *self)
{
    if (self != NULL && self->mem != NULL) {
        mmio_unmap(self->mem, SPILED_HWSIZE);
        self->mem = NULL;
    }
}


////////////////////////////////////////////////////////////////
// HW interop
//

void encoder_hw_update(encoders *self)
{
    input_ll_state *state = &self->hw_ll;
    uint32_t reg = mmio_read32(self->mem, SPILED_KNOBS);

    state->value[KNOB_R] = (reg >> SPILED_RED_KNOB_CNT_OFF)   & SPILED_KNOB_CNT_MASK;
    state->value[KNOB_G] = (reg >> SPILED_GREEN_KNOB_CNT_OFF) & SPILED_KNOB_CNT_MASK;
    state->value[KNOB_B] = (reg >> SPILED_BLUE_KNOB_CNT_OFF)  & SPILED_KNOB_CNT_MASK;
    encoder_st_press(state, KNOB_R, (reg & SPILED_RED_KNOB_PRESSED)    != 0);
    encoder_st_press(state, KNOB_G, (reg & SPILED_GREEN_KNOB_PRESSED)  != 0);
    encoder_st_press(state, KNOB_B, (reg & SPILED_BLUE_KNOB_PRESSED)   != 0);
}
