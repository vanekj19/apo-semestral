/**
 * @file build_config.h
 * @brief Program-wide build-time configuration
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 */

#ifndef BUILD_CONFIG_H
#define BUILD_CONFIG_H

#define APP_VERSION "1.0"      //!< Program version
#define DEBUG_LOG_ALWAYS true  //!< Whether debug output is on by default (so always on)
#define DEFAULT_LANG_CZ        //!< Whether strings are in czech by default

#define RGB_COLOR_UNLOCK_FAIL  0x00FF0000 //!< RGB LED color when an incorrect code is entered
#define RGB_COLOR_UNLOCKED     0x0000FF00 //!< RGB LED color when the lock is unlocked
#define RGB_COLOR_LOCKED       0x000000FF //!< RGB LED color when the lock is locked

#define LED_ANIMATION_PATTERN   0xF0000000 //!< LED line pattern that goes around
#define LED_UNLOCK_FAIL_PATTERN 0xFFFFFFFF //!< LED line pattern to show when an incorrect code is entered

#define ANIMATION_PERIOD 4000 //!< Idle LED line animation period (msec)
#define UNLOCK_WARN      1000 //!< How long to display warning after an incorrect code is entered (msec)
#define GUI_FPS            10 //!< How often to update the UI (Hz/fps)

#define MOTOR_PWM_FREQ  20000  //!< How fast to make one motor PWM cycle (Hz)
#define MOTOR_REG_FREQ    5000 //!< How often to run the regulation code in the regulation thread (Hz)
#define MOTOR_REQ_PRIORITY  50 //!< What Linux realtime priority to use for the regulation thread

#define MAX_CONFIG_STR_LEN  64 //! Maximum length of strings in config file (bytes)

#endif // BUILD_CONFIG_H
