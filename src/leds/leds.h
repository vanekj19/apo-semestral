/**
 * @file leds.h
 * @brief module for interfacing with MZ_APO yellow and RGB LEDs
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup leds
 */

#ifndef LEDS_INTERFACE_H
#define LEDS_INTERFACE_H

#include "core/common.h"
#include "core/mmio_regs.h"
#include "core/defs.h"

/**
 * @defgroup leds LED module
 * @brief Animation and control of MZ_APO LEDs
 *
 * This module is a wrapper around the LED part of the MZ_APO SPILED peripheral.
 * It provides a high-level API based around time-based animations and
 * application states. However, for the yellow LED line a raw write
 * operation is made available too.
 * @{
 */

//! Number of steps in the LED animation
#define LED_ANIMATION_STEPS 32

//! Number of yellow LEDs
#define YLEDS 32

/**
 * @name Init/deinit
 * @{
 */

/**
 * @brief Map the LED controller hardware into the program's virtual memory.
 * @returns LED controller address or NULL if the mapping failed.
 */
extern led_if *led_init(void);

/**
 * @brief Deinitialize & unmap the LED controller.
 * @param hw LED controller address.
 */
extern void led_deinit(led_if *hw);

/**@}*/
/**
 * @name LED output
 * @{
 */

/**
 * @brief Show a pattern directly on the yellow LEDs.
 * @param leds    LED controller address.
 * @param pattern Pattern to show.
 */
extern void led_line_draw_raw(led_if *leds, uint32_t pattern);

/**
 * @brief Draw an idle animation on the yellow LEDs.
 *
 * @verbatim
 * The animation will look like this:
 * +--------------------------------+
 * |   XXXX                         |
 * +--------------------------------+
 *      -> movement direction ->
 * @endverbatim
 *
 * @param leds            LED controller address.
 * @param animation_step  Animation counter from which to derive the
 *                        actual LED state.
 */
extern void led_line_draw_animation(led_if *leds, int animation_step);

/**
 * @brief Draw a timeout "progressbar" on the yellow LEDs.
 *
 * @verbatim
 * The progressbar will look like this:
 * +--------------------------------+
 * |XXXXXXXXXXX                     |
 * +--------------------------------+
 *  <- leds  ->
 * @endverbatim
 *
 * @param leds            LED controller address.
 * @param remaining_leds  How many LEDs should remain on.
 */
extern void led_line_draw_timeout(led_if *leds, int remaining_leds);

/**
 * @brief Set RGB LED color to match the current UI state.
 *
 * The color mapping is as follows:
 * - locked   => blue
 * - failure  => red
 * - unlocked => green
 *
 * @param leds   LED controller address.
 * @param state  Current UI state (locked, unlocked, error).
 */
extern void led_rgb_set(led_if *leds, ui_state state);

/**@}*/
/**@}*/

#endif // LEDS_INTERFACE_H
