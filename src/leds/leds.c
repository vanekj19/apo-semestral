/**
 * @file leds.c
 * @brief module for interfacing with MZ_APO yellow and RGB LEDs
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup leds
 */

#include "build_config.h"
#include "leds/leds.h"
#include "core/logging.h"
#include "core/mmio_regs.h"
#include "core/mmio_map.h"

/**
 * @addtogroup leds
 * @{
 */

//! Register value for OFF leds
#define RGB_COLOR_OFF 0x00000000

////////////////////////////////////////////////////////////////
// Initialization/deinitialization
//

led_if *led_init(void)
{
    led_if *mem = (led_if *) mmio_map(SPILED_HWADDR, SPILED_HWSIZE, false);
    if (mem == NULL) {
        return NULL;
    }

    mmio_write32(mem, SPILED_LINE,  0);
    mmio_write32(mem, SPILED_RGB_L, RGB_COLOR_OFF);
    mmio_write32(mem, SPILED_RGB_R, RGB_COLOR_OFF);
    return mem;
}

void led_deinit(led_if *hw)
{
    if (hw != NULL) {
        mmio_write32(hw, SPILED_LINE,  0);
        mmio_write32(hw, SPILED_RGB_L, RGB_COLOR_OFF);
        mmio_write32(hw, SPILED_RGB_R, RGB_COLOR_OFF);
        mmio_unmap(hw, SPILED_HWSIZE);
    }
}


////////////////////////////////////////////////////////////////
// LED line drawing utilities
//

void led_line_draw_raw(led_if *leds, uint32_t pattern)
{
    mmio_write32(leds, SPILED_LINE, pattern);
}

void led_line_draw_animation(led_if *leds, int animation_step)
{
    animation_step = animation_step % LED_ANIMATION_STEPS;

    int shift_l = animation_step;
    int shift_r = YLEDS - animation_step;

    uint32_t base = LED_ANIMATION_PATTERN;
    uint32_t hi   = shift_l < 32 ? (base >> shift_l) : 0;
    uint32_t lo   = shift_r < 32 ? (base << shift_r) : 0;
    uint32_t final = hi | lo;

    mmio_write32(leds, SPILED_LINE, final);
}

void led_line_draw_timeout(led_if *leds, int remaining_leds)
{
    remaining_leds = clip(remaining_leds, 0, YLEDS);

    int off_leds = YLEDS - remaining_leds;

    uint32_t off_bits = off_leds < 32 ? ((1u << off_leds) - 1u) : (~0u);
    uint32_t on_bits  = ~off_bits;

    mmio_write32(leds, SPILED_LINE, on_bits);
}


////////////////////////////////////////////////////////////////
// RGB LED utilities
//

void led_rgb_set(led_if *leds, ui_state state)
{
    uint32_t color;
    switch (state) {
    case LOCK_LOCKED:        color = RGB_COLOR_LOCKED;      break;
    case LOCK_UNLOCK_FAILED: color = RGB_COLOR_UNLOCK_FAIL; break;
    case LOCK_UNLOCKED:      color = RGB_COLOR_UNLOCKED;    break;
    default:                 color = RGB_COLOR_OFF;         break;
    }

    mmio_write32(leds, SPILED_RGB_L, color);
    mmio_write32(leds, SPILED_RGB_R, color);
}

/**@}*/
