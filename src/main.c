/**
 * @file main.c
 * @brief Application entry point
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup main
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#include "config/config.h"
#include "core/logging.h"
#include "core/mmio_map.h"
#include "core/mmio_regs.h"
#include "logic/logic.h"
#include "args.h"

/**
 * @defgroup main Entrypoint
 * @brief Application entry point
 *
 * This module glues together the initialization and deinitialization
 * phases of program execution.
 */

//! Global program state
static program_state state;

//! Register SIGINT/SIGTERM signal handlers
static bool init_signals(void);

//! Stop application as a response to SIGINT/SIGTERM.
static void sighandler(int signo);


////////////////////////////////////////////////////////////////
// Program entry point
//

/**
 * @ingroup main
 */
int main(int argc, char *argv[])
{
    CONFIG = DEFAULT_CONFIG;
    bool colorize = isatty(STDOUT_FILENO);
    log_init(colorize);

    argcmd cmd = args_process(argc, argv);
    if (cmd == ARGS_EXIT) {
        return 0;
    } else if (cmd == ARGS_ERROR) {
        return 1;
    }

    log_info("starting lock v" APP_VERSION);

    bool ok = program_init(&state);
    ok &= init_signals();

    if (ok) {
        log_debug("entering program");

        program_enter(&state);
    } else {
        log_error("program initialization failed");
    }

    log_info("exiting");
    program_deinit(&state);
    return ok ? 0 : 1;
}


////////////////////////////////////////////////////////////////
// Signal handling
//

bool init_signals(void)
{
    struct sigaction act;
    act.sa_handler = sighandler;

    if (sigaction(SIGINT,  &act, NULL) < 0) {
        log_sys_error("cannot configure SIGINT handler");
        return false;
    }

    if (sigaction(SIGTERM, &act, NULL) < 0) {
        log_sys_error("cannot configure SIGTERM handler");
        return false;
    }
    return true;
}

void sighandler(int signo)
{
    state.do_run = false;
}
