/**
 * @file lcd.h
 * @brief Public API of the LCD module
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup lcd
 */

#ifndef LCD_INTERFACE_H
#define LCD_INTERFACE_H

#include "core/common.h"
#include "core/mmio_regs.h"
#include "core/defs.h"

/**
 * @defgroup lcd LCD module
 * @brief LCD hardware management
 *
 * This module provides a wrapper around the LCD controller peripheral.
 * It handles the low-level details of LCD initialization & image
 * transport.
 * @{
 */

/**
 * @brief Map the LCD controller hardware and initialize it.
 * @returns LCD controller address or NULL if the mapping failed.
 */
extern lcd_if *lcd_init(void);

/**
 * @brief Deinitialize the LCD controller and unmap it from the program's address space.
 * @param hw LCD controller addresss.
 */
extern void lcd_deinit(lcd_if *hw);

/**
 * @brief Write the specified frame onto the LCD screen surface.
 * @param lcd   LCD controller with which to render the screen.
 * @param frame Frame to write to the LCD.
 */
extern void lcd_write(lcd_if *lcd, lcd_frame *frame);

/**@}*/
#endif // LCD_INTERFACE_H
