/**
 * @file lcd.c
 * @brief Low-level interaction with MZ_APO LCD peripheral
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup lcd
 */

#include <stdint.h>
#include <time.h>

#include "lcd/lcd.h"
#include "core/mmio_regs.h"
#include "core/mmio_map.h"
#include "core/common.h"
#include "config/config.h"

//! Two black RGB565 pixels in one 32bit word
#define BIPIXEL_BLACK 0x00000000

//! Initialize the attached LCD panel
static void lcd_panel_init(lcd_if *ctrl);

//! Fill the panel with the specified two pixels.
static void lcd_clear(lcd_if *lcd, uint32_t bipixel);

//! Initialize ILI9481 panel controller
static void lcd_init_ili9481(lcd_if *ctrl);

//! Initialize HX8357-C panel controller
static void lcd_init_hx8357c(lcd_if *ctrl);


////////////////////////////////////////////////////////////////
// Initialization/deinitialization
//

lcd_if *lcd_init(void)
{
    lcd_if *mem = mmio_map(PARLCD_HWADDR, PARLCD_HWSIZE, false);
    if (mem == NULL) {
        return NULL;
    }

    lcd_panel_init(mem);
    lcd_clear(mem, BIPIXEL_BLACK);
    return mem;
}

void lcd_deinit(lcd_if *hw)
{
    if (hw != NULL) {
        lcd_clear(hw, BIPIXEL_BLACK);
        mmio_unmap(hw, PARLCD_HWSIZE);
    }
}


////////////////////////////////////////////////////////////////
// LCD pixel pipes
//

void lcd_write(lcd_if *lcd, lcd_frame *frame)
{
    uint32_t *base = (uint32_t *) &frame->pixels;
    int loops = LCD_WIDTH * LCD_HEIGHT * sizeof(rgb565) / sizeof(uint32_t);

    mmio_write16(lcd, PARLCD_COMMAND, 0x2C);
    for (int i = 0; i < loops; i++) {
        mmio_write32(lcd, PARLCD_WRITE_32, base[i]);
    }
}

void lcd_clear(lcd_if *lcd, uint32_t bipixel)
{
    int loops = LCD_WIDTH * LCD_HEIGHT * sizeof(rgb565) / sizeof(uint32_t);

    mmio_write16(lcd, PARLCD_COMMAND, 0x2C);
    for (int i = 0; i < loops; i++) {
        mmio_write32(lcd, PARLCD_WRITE_32, bipixel);
    }
}


////////////////////////////////////////////////////////////////
// Panel-specific setup
//

void lcd_panel_init(lcd_if *ctrl)
{
    mmio_write16(ctrl, PARLCD_COMMAND, PARLCD_CMD_RESET);
    delay_ms(30);

    if (CONFIG.new_display) {
        lcd_init_ili9481(ctrl);
    } else {
        lcd_init_hx8357c(ctrl);
    }
}


void lcd_init_ili9481(lcd_if *ctrl)
{
    // Configure ILI9481 display

    mmio_write16(ctrl, PARLCD_COMMAND, 0x11);
    delay_ms(20);
    mmio_write16(ctrl, PARLCD_COMMAND, 0xD0);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x07);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x42);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x18);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xD1);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x07);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x10);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xD2);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x01);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x02);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xC0);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x10);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x3B);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x02);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x11);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xC5);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x03);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xC8);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x32);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x36);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x45);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x06);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x16);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x37);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x75);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x77);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x54);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x0C);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);

    mmio_write16(ctrl, PARLCD_COMMAND, 0x36);
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x0A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x28);

    mmio_write16(ctrl, PARLCD_COMMAND, 0x3A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x55);

    //mmio_write16(ctrl, PARLCD_COMMAND, 0x2A);
    mmio_write16(ctrl, PARLCD_COMMAND,  0x2B);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x01);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x3F);

    //mmio_write16(ctrl, PARLCD_COMMAND, 0x2B);
    mmio_write16(ctrl, PARLCD_COMMAND,  0x2A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x01);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0xDF);

    delay_ms(120);
    mmio_write16(ctrl, PARLCD_COMMAND, 0x29);

    delay_ms(25);
}

void lcd_init_hx8357c(lcd_if *ctrl)
{
    // HX8357-C display initialisation

    mmio_write16(ctrl, PARLCD_COMMAND, 0xB9); // Enable extension command
    mmio_write16(ctrl, PARLCD_WRITE_16, 0xFF);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x83);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x57);
    delay_ms(50);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xB6); //Set VCOM voltage
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x2C);    //0x52 for HSD 3.0"
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x52);    //0x52 for HSD 3.0"

    mmio_write16(ctrl, PARLCD_COMMAND, 0x11); // Sleep off
    delay_ms(200);

    mmio_write16(ctrl, PARLCD_COMMAND, 0x35); // Tearing effect on
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);    // Added parameter

    mmio_write16(ctrl, PARLCD_COMMAND, 0x3A); // Interface pixel format
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x55);    // 16 bits per pixel

    //mmio_write16(ctrl, PARLCD_COMMAND, 0xCC); // Set panel characteristic
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x09);    // S960>S1, G1>G480, R-G-B, normally black

    //mmio_write16(ctrl, PARLCD_COMMAND, 0xB3); // RGB interface
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x43);
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x06);
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x06);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xB1); // Power control
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x15);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x0D);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x0D);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x83);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x48);


    mmio_write16(ctrl, PARLCD_COMMAND, 0xC0); // Does this do anything?
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x24);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x24);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x01);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x3C);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0xC8);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x08);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xB4); // Display cycle
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x02);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x40);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x2A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x2A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x0D);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x4F);

    mmio_write16(ctrl, PARLCD_COMMAND, 0xE0); // Gamma curve
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x15);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x1D);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x2A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x31);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x42);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x4C);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x53);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x45);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x40);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x3B);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x32);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x2E);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x28);

    mmio_write16(ctrl, PARLCD_WRITE_16, 0x24);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x03);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x15);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x1D);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x2A);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x31);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x42);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x4C);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x53);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x45);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x40);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x3B);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x32);

    mmio_write16(ctrl, PARLCD_WRITE_16, 0x2E);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x28);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x24);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x03);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x00);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0x01);

    mmio_write16(ctrl, PARLCD_COMMAND, 0x36); // MADCTL Memory access control
    //mmio_write16(ctrl, PARLCD_WRITE_16, 0x48);
    mmio_write16(ctrl, PARLCD_WRITE_16, 0xE8);
    delay_ms(20);

    mmio_write16(ctrl, PARLCD_COMMAND, 0x21); //Display inversion on
    delay_ms(20);

    mmio_write16(ctrl, PARLCD_COMMAND, 0x29); // Display on

    delay_ms(120);
}
