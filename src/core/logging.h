/**
 * @file logging.h
 * @brief logging utility headers
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logging
 */

#ifndef LOGGING_H
#define LOGGING_H

#include <stdbool.h>

/**
 * @defgroup logging Logging module
 * @ingroup core
 * @brief Utilities for displaying debug/info/... messages to the user
 *
 * This module provides several functions for simplifying logging. These fall
 * into the following classes:
 *  - plain logging
 *  - logging with errno handling
 *  - fatal assertions
 *  - abort() wrapping
 *
 * To simplify the syntax, a set of helper macros is provided that feed
 * the source code location & executing function into the required functions.

 * Most of the macros behave exactly like printf - you can provide a format
 * string in the first argument and its contents in the following arguments.
 *
 * @{
 */

#ifndef NO_TRACE_LOG
//! Print a tracing message to stderr
#define log_trace(...) log_plain(LEVEL_TRACE, __func__, __FILE__, __LINE__, __VA_ARGS__)
#else
//! Print a tracing message to stderr
#define log_trace(...) do {} while(0)
#endif

#ifndef NO_DEBUG_LOG
//! Print a debugging message to stderr
#define log_debug(...)  log_plain(LEVEL_DEBUG,   __func__, __FILE__, __LINE__, __VA_ARGS__)
#else
//! Print a debugging message to stderr
#define log_debug(...) do {} while(0)
#endif

//! Print an informational message to stderr
#define log_info(...)   log_plain(LEVEL_INFO,    __func__, __FILE__, __LINE__, __VA_ARGS__)

//! Print a warning message to stderr
#define log_warn(...)   log_plain(LEVEL_WARNING, __func__, __FILE__, __LINE__, __VA_ARGS__)

//! Print an error message to stderr
#define log_error(...)  log_plain(LEVEL_ERROR,   __func__, __FILE__, __LINE__, __VA_ARGS__)


//! Print an informational message to stderr; include the current errno state
#define log_sys_info(...)  log_errno(LEVEL_INFO,    __func__, __FILE__, __LINE__, __VA_ARGS__)

//! Print a warning message to stderr; include the current errno state
#define log_sys_warn(...)  log_errno(LEVEL_WARNING, __func__, __FILE__, __LINE__, __VA_ARGS__)

//! Print an error message to stderr; include the current errno state
#define log_sys_error(...) log_errno(LEVEL_ERROR,   __func__, __FILE__, __LINE__, __VA_ARGS__)

//! Assert that a condition holds and abort the program if it doesn't
#define log_assert(cond, ...) do { \
        if (!(cond)) { \
            log_error("assertion failure: " __VA_ARGS__); \
            log_abort(); \
        } \
    } while(0)

//! Marker for states that must not happen in a bug-free program
#define log_unreachable(...) do { \
        log_error("reached unreachable point: " __VA_ARGS__); \
        log_abort(); \
    } while(0)

//! Message severity
typedef enum {
    LEVEL_TRACE,   //!< On-demand messages for in-depth analysis of some situation
    LEVEL_DEBUG,   //!< Casual debugging output useful for debugging less complicated situations
    LEVEL_INFO,    //!< Informational output for the user
    LEVEL_WARNING, //!< Non-fatal issue detected
    LEVEL_ERROR    //!< Fatal issue detected
} severity;

/**
 * @brief Enable or disable debug logging.
 * @param on Whether to enable or disable it.
 */
extern void log_enable_debug(bool on);

/**
 * @brief Initialize logging backend.
 * @param colorize Whether to enable colored marking of different log levels.
 */
extern void log_init(bool colorize);

/**
 * @brief Log a message to stderr.
 * @param level    How severe is the situation that caused the message
 * @param function Function from which the message originates
 * @param file     File from which the message originates
 * @param line     Line number in the file from which the message originates
 * @param fmt      printf-style format string
 */
extern void log_plain(severity level,
                      const char *function,
                      const char *file,
                      int line,
                      const char *fmt, ...) __attribute__((format(printf, 5, 6)));

/**
 * @brief Log a message to stderr and include errno description.
 * @param level    How severe is the situation that caused the message
 * @param function Function from which the message originates
 * @param file     File from which the message originates
 * @param line     Line number in the file from which the message originates
 * @param fmt      printf-style format string
 */
extern void log_errno(severity level,
                      const char *function,
                      const char *file,
                      int line,
                      const char *fmt, ...) __attribute__((format(printf, 5, 6)));

/**
 * @brief Abort program execution.
 * @remark Never returns (suprisingly.)
 */
extern void log_abort(void) __attribute__((noreturn));

/**
 * @}
 */

#endif // LOGGING_H
