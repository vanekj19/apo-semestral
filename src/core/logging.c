/**
 * @file logging.c
 * @brief logging utility implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logging
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>
#include <sys/prctl.h>

#include "logging.h"
#include "common.h"
#include "build_config.h"

////////////////////////////////////////////////////////////////
// Initial defs/decls
//

/**
 * @brief Output format string
 * - arg 1: color start
 * - arg 2: loglevel name
 * - arg 3: color end
 * - arg 4: time since program start in seconds
 * - arg 5: calling thread name
 * - arg 6: calling function name
 * - arg 7: formatted message string
 * - arg 8: calling file name
 * - arg 9: calling line number
 */
#define LOG_FORMAT "%s[%s]%s %8.3f @%-10s %20s: %s (%s:%d)\n"

//! Maximum length of thread name (prctl returns only 16 bytes)
#define MAX_THREAD_NAME 16

// helper macros for output colorization
#define ESC             "\x1B"
#define RESET           ESC "[m"
#define BRIGHT_RED      ESC "[1;31m"
#define BRIGHT_GREEN    ESC "[1;32m"
#define BRIGHT_YELLOW   ESC "[1;33m"
#define BRIGHT_BLUE     ESC "[1;34m"
#define GRAY            ESC "[1;30m"

/**
 * @brief Log an unformatted line to stderr.
 * @param level    Severity of the event that triggered this message
 * @param function Function in which the message was printed
 * @param file     File in which the message was printed
 * @param line     Line in which the message was printed
 * @param str      Message contents
 */
static void log_direct(severity level, const char *function,
                       const char *file, int line, const char *str);

/**
 * @brief Get time offset from program start
 * @returns Seconds elapsed since the logging module was initialized
 */
static float get_time(void);

/**
 * @brief Get current thread name.
 * @param[out] array Destination buffer
 * @param      bytes Capacity of the destination buffer
 * @remark This function cannot fail; on failure it just writes a dummy
 *         string into the output buffer.
 */
static void get_thread_name(char *array, size_t bytes);


////////////////////////////////////////////////////////////////
// Global data
//

//! ANSI escape sequence for starting color output at given severity
static const char *level_colors[] = {
    [LEVEL_TRACE]   = GRAY,
    [LEVEL_DEBUG]   = BRIGHT_BLUE,
    [LEVEL_INFO]    = BRIGHT_GREEN,
    [LEVEL_WARNING] = BRIGHT_YELLOW,
    [LEVEL_ERROR]   = BRIGHT_RED,
};

//! Names of the loglevels
static const char *level_names[] = {
    [LEVEL_TRACE]   = "TRACE",
    [LEVEL_DEBUG]   = "DEBUG",
    [LEVEL_INFO]    = "INFO ",
    [LEVEL_WARNING] = "WARN ",
    [LEVEL_ERROR]   = "ERROR",
};

//! CLOCK_MONOTONIC timestamp from program startup
static struct timespec time_t0;

//! Flag indicating whether severity level is printed in color
static bool enable_color;

//! Flag indicating whether TRACE and DEBUG severities do anything
//! This may be overriden by NO_*_LOG; in that case the logging
//! functions are not even compiled in
static bool debug_enabled = DEBUG_LOG_ALWAYS;


////////////////////////////////////////////////////////////////
// Helper functions
//

void log_init(bool colorize)
{
    enable_color = colorize;
    time_t0 = now_monotonic();
}

void log_abort(void)
{
    fflush(stdout);
    fflush(stderr);
    abort();
}

void log_enable_debug(bool on)
{
    debug_enabled = on;
}

////////////////////////////////////////////////////////////////
// Logging functions
//

void log_plain(severity level,
               const char *function,
               const char *file,
               int line,
               const char *fmt, ...)
{
    char *user_str = NULL;

    va_list args;
    va_start(args, fmt);
    int user_ret = vasprintf(&user_str, fmt, args);
    va_end(args);

    if (user_ret != -1) {
        log_direct(level, function, file, line, user_str);
        free(user_str);
    }
}

void log_errno(severity level,
               const char *function,
               const char *file,
               int line,
               const char *fmt, ...)
{
    char *user_str = NULL;

    int error = errno;

    va_list args;
    va_start(args, fmt);
    int status = vasprintf(&user_str, fmt, args);
    va_end(args);

    if (status >= 0) {
        char *wrapped_str = NULL;
        status = asprintf(&wrapped_str, "%s [error %d: %s]",
            user_str, error, strerror(error));

        if (status >= 0) {
            log_direct(level, function, file, line, wrapped_str);
        }
        free(wrapped_str);
    }

    free(user_str);
    errno = error;
}

void log_direct(severity level, const char *function,
                       const char *file, int line, const char *str)
{
    if (debug_enabled == false) {
        // skip if not enabled
        if (level == LEVEL_DEBUG || level == LEVEL_TRACE) {
            return;
        }
    }
    const char *color_start = enable_color ? level_colors[level] : "";
    const char *color_end   = enable_color ? RESET               : "";

    char thread_name[MAX_THREAD_NAME];
    get_thread_name(thread_name, MAX_THREAD_NAME);

    float time = get_time();

    fprintf(stderr, LOG_FORMAT,
        color_start,
        level_names[level],
        color_end,
        time, thread_name,
        function, str, file, line);
}


////////////////////////////////////////////////////////////////
// Helper functions
//

float get_time(void)
{
    return millis_from(&time_t0) / 1000.0f;
}

void get_thread_name(char *array, size_t bytes)
{
    if (bytes < 16) {
        snprintf(array, bytes, "<OVERFLOWED>");
        return;
    }

    int status = prctl(PR_GET_NAME, array);
    if (status < 0) {
        strncpy(array, "<unknown>", bytes);
    }
}
