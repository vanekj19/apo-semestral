/**
 * @file common.c
 * @brief implementation of timing/numeric utility functions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup common
 */

#include "core/common.h"

/**
 * @addtogroup common
 * @{
 */

//! Helper constant with the number of nanoseconds in a second.
#define NANOS_IN_SECOND 1000000000


////////////////////////////////////////////////////////////////
// Number clipping helpers
//

int clip(int value, int min, int max)
{
    if (value < min) {
        return min;
    }
    if (value > max) {
        return max;
    }
    return value;
}

float clipf(float value, float min, float max)
{
    if (value < min) {
        return min;
    }
    if (value > max) {
        return max;
    }
    return value;
}


////////////////////////////////////////////////////////////////
// Time manipulation helpers
//

void delay_ms(int msec)
{
    struct timespec wait_delay_ms = {
        .tv_sec = msec / 1000,
        .tv_nsec = (msec % 1000) * 1000 * 1000
    };
    clock_nanosleep(CLOCK_MONOTONIC, 0, &wait_delay_ms, NULL);
}

void loop_delay(struct timespec *last_iter, int frequency)
{
    last_iter->tv_nsec += NANOS_IN_SECOND / frequency;
    while (last_iter->tv_nsec >= NANOS_IN_SECOND) {
        last_iter->tv_nsec -= NANOS_IN_SECOND;
        last_iter->tv_sec  += 1;
    }

    while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, last_iter, NULL) != 0) {}
}

long millis_from(struct timespec *start)
{
    struct timespec now = now_monotonic();

    long sec_diff  = now.tv_sec  - start->tv_sec;
    long nsec_diff = now.tv_nsec - start->tv_nsec;

    if (nsec_diff < 0) {
        nsec_diff += 1000000000;
        sec_diff  -= 1;
    }

    return sec_diff * 1000L + nsec_diff / 1000000L;
}

int time_compare(struct timespec *a, struct timespec *b)
{
    if (a->tv_sec < b->tv_sec) {
        return -1;
    }
    if (a->tv_sec > b->tv_sec) {
        return +1;
    }
    if (a->tv_nsec < b->tv_nsec) {
        return -1;
    }
    if (a->tv_nsec > b->tv_nsec) {
        return +1;
    }
    return 0;
}

struct timespec now_monotonic(void)
{
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return now;
}

/**@}*/
