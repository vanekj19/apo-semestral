/**
 * @file mmio_regs.h
 * @brief definition of the MZ_APO design registers
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup mmio
 */

#ifndef CORE_MMIO_REGS_H
#define CORE_MMIO_REGS_H

#include <stdint.h>
#include <stdlib.h>
#include "core/mmio_map.h"

/**
 * @defgroup mmio MMIO utilities
 * @ingroup core
 * @brief MMIO mapping and accessing functions, including register layouts
 *
 * This module provides macros and functions for accessing memory mapped
 * peripherals.
 *
 * Complete description of the educational MZ_APO design registers
 * can be found at https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo/start
 *
 * The peripherals VHDL sources can be found in the repository
 * http://rtime.felk.cvut.cz/gitweb/fpga/zynq/canbench-sw.git/tree/refs/heads/microzed_apo:/system/ip
 *
 * @{
 */


////////////////////////////////////////////////////////////////
// I/O accessor functions
//

/** @name MMIO reads
 * @brief Perform a MMIO read including all necessary memory barriers
 * @param base   Base address of the MMIO region.
 * @param offset Byte offset within the region.
 * @returns Value at the appropriate address.
 */
/**@{*/
extern uint8_t  mmio_read8(mmio_region *base, size_t offset);
extern uint16_t mmio_read16(mmio_region *base, size_t offset);
extern uint32_t mmio_read32(mmio_region *base, size_t offset);
/**@}*/

/** @name MMIO writes
 * @brief Perform a MMIO write including all necessary memory barriers
 * @param base   Base address of the MMIO region.
 * @param offset Byte offset within the region.
 * @param value  Value to write at the appropriate address.
 */
/**@{*/
extern void mmio_write8(mmio_region *base, size_t offset,  uint8_t value);
extern void mmio_write16(mmio_region *base, size_t offset, uint16_t value);
extern void mmio_write32(mmio_region *base, size_t offset, uint32_t value);
/**@}*/

/** @name MMIO typedefs
 * @brief Helper aliases for MMIO region addresses
 */
/**@{*/
typedef mmio_region led_if;
typedef mmio_region enc_if;
typedef mmio_region lcd_if;
typedef mmio_region motor_if;
/**@}*/


/** @name SPILED registers
 * @brief SPILED controller MMIO definitions
 */
/**@{*/

#define SPILED_HWADDR  0x43c40000UL //!< Physical memory address of the SPILED peripheral
#define SPILED_HWSIZE  0x00004000UL //!< Size of the SPILED memory region

#define SPILED_RGB_RED_OFF          16        //!< Offset of the red component within RGB led color (SPILED_RGB_*)
#define SPILED_RGB_BLUE_OFF          8        //!< Offset of the green component within RGB led color (SPILED_RGB_*)
#define SPILED_RGB_GREEN_OFF         0        //!< Offset of the blue component within RGB led color (SPILED_RGB_*)
#define SPILED_RGB_MASK           0xFFu       //!< Mask for one RGB component when shifted to bit 0 (SPILED_RGB_*)
#define SPILED_RED_KNOB_CNT_OFF     16        //!< Offset of red knob rotation (SPILED_KNOBS)
#define SPILED_GREEN_KNOB_CNT_OFF    8        //!< Offset of green knob rotation (SPILED_KNOBS)
#define SPILED_BLUE_KNOB_CNT_OFF     0        //!< Offset of blue knob rotation (SPILED_KNOBS)
#define SPILED_KNOB_CNT_MASK      0xFFu       //!< Mask of one knob rotation when shifted to bit 0 (SPILED_KNOBS)
#define SPILED_RED_KNOB_PRESSED   (1u << 26u) //!< Bitmask indicating whether red knob is pressed (SPILED_KNOBS)
#define SPILED_GREEN_KNOB_PRESSED (1u << 25u) //!< Bitmask indicating whether green knob is pressed (SPILED_KNOBS)
#define SPILED_BLUE_KNOB_PRESSED  (1u << 24u) //!< Bitmask indicating whether blue knob is pressed (SPILED_KNOBS)

#define SPILED_LINE      0x04 //!< Register: Yellow LED line state
#define SPILED_RGB_L     0x10 //!< Register: Left  RGB LED state
#define SPILED_RGB_R     0x14 //!< Register: Right RGB LED state
#define SPILED_KBDWR     0x18 //!< Register: Keyboard control register
#define SPILED_KNOBS_RAW 0x20 //!< Register: Knob unfiltered state
#define SPILED_KNOBS     0x24 //!< Register: Knob decoder register

/**@}*/


/** @name PARLCD registers
 * @brief PARLCD (parallel LCD) controller MMIO definitions
 */
/**@{*/

#define PARLCD_HWADDR  0x43c00000UL //!< Physical memory address of the PARLCD peripheral
#define PARLCD_HWSIZE  0x00004000UL //!< Size of the PARLCD memory region

#define PARLCD_RESET  (1u << 1u) //!< Bitmask for raising the reset line on PARLCD
#define PARLCD_CMD_RESET  0x01 //!< Common reset command for PARLCD panels

#define PARLCD_CONTROL    0x00 //!< Register: LCD bridge control register
#define PARLCD_COMMAND    0x08 //!< Register: LCD bridge command tx
#define PARLCD_WRITE_16   0x0C //!< Register: LCD bridge one-pixel tx
#define PARLCD_WRITE_32   0x0C //!< Register: LCD bridge two-pixel tx

/**@}*/


/** @name Motor ctrl registers
 * @brief Motor controller controller MMIO definitions
 */
/**@{*/

#define MOTOR_HWADDR  0x43c20000UL //!< Physical memory address of the motor controller peripheral
#define MOTOR_HWSIZE  0x00004000UL //!< Size of the motor controller memory region

#define MOTOR_CTRL_PWM_A_DIRECT (1u << 4u)  //!< Turn on PWM A channel directly (MOTOR_CONTROL)
#define MOTOR_CTRL_PWM_B_DIRECT (1u << 5u)  //!< Turn on PWM B channel directly (MOTOR_CONTROL)
#define MOTOR_CTRL_PWM_ENABLE   (1u << 6u)  //!< Enable PWM output (MOTOR_CONTROL)
#define MOTOR_CTRL_IRC_RESET    (1u << 8u)  //!< Reset IRC (MOTOR_CONTROL)
#define MOTOR_STATUS_IRC_A      (1u <<  8u) //!< Direct reading of IRC A channel (MOTOR_STATUS)
#define MOTOR_STATUS_IRC_B      (1u <<  9u) //!< Direct reading of IRC B channel (MOTOR_STATUS)
#define MOTOR_STATUS_IRQ        (1u << 10u) //!< Direct reading of IRC IRQ channel (MOTOR_STATUS)
#define MOTOR_PERIOD_MASK       0x3fffffffu //!< Mask of the valid PWM period bits (MOTOR_PERIOD)
#define MOTOR_DUTY_MASK         0x3fffffffu //!< Mask of the valid PWM duty cycle bits (MOTOR_DUTY)
#define MOTOR_DUTY_FWD          (1u << 30u) //!< Apply the PWM in forward direction (MOTOR_DUTY)
#define MOTOR_DUTY_BWD          (1u << 31u) //!< Apply the PWM in reverse direction (MOTOR_DUTY)

#define MOTOR_TIMER_CLOCK       100000000  //!< Input clock frequency in Hz

#define MOTOR_CONTROL    0x00 //!< Register: Control register
#define MOTOR_STATUS     0x04 //!< Register: Status register
#define MOTOR_PERIOD     0x08 //!< Register: PWM total period in timer ticks (reference counter runs at MOTOR_TIMER_CLOCK)
#define MOTOR_DUTY       0x0C //!< Register: PWM on period in timer ticks (reference counter runs at MOTOR_TIMER_CLOCK)
#define MOTOR_IRC        0x10 //!< Register: IRC counter/decoder value

/**@}*/
/**@}*/

#endif // CORE_MMIO_REGS_H
