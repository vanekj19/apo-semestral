/**
 * @file defs.h
 * @brief Shared type definitions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup common_defs
 */

#ifndef CORE_DEFS_H
#define CORE_DEFS_H

/**
 * @defgroup common_defs Program-wide type definitions
 * @ingroup core
 * @brief Typedef common to the whole program
 * @{
 */

//! Width of the LCD panel in pixels
#define LCD_WIDTH   480

//! Height of the LCD panel in pixels
#define LCD_HEIGHT  320

/**
 * @brief Current lock UI status.
 */
typedef enum {
    LOCK_LOCKED,        //!< Lock is locked, idle mode
    LOCK_UNLOCK_FAILED, //!< Lock is locked, warning after unsuccessful unlock
    LOCK_UNLOCKED       //!< Lock is unlocked
} ui_state;

//! Data type for representing RGB565 pixels.
typedef uint16_t rgb565;

//! Full LCD frame.
typedef struct {
    rgb565 pixels[LCD_HEIGHT][LCD_WIDTH];
} lcd_frame __attribute__((aligned(4)));

/**@}*/
#endif // CORE_DEFS_H
