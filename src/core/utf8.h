/**
 * @file utf8.h
 * @brief UTF-8 codec headers
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup utf8
 */

#ifndef CORE_UTF8_H
#define CORE_UTF8_H

#include <uchar.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * @defgroup utf8 UTF-8 codec
 * @ingroup core
 * @brief Converter between UTF-8 and UTF-32
 *
 * This module provides a lightweight UTF-8 <-> UTF-32 converter.
 * This is used for enabling the rendering of non-ASCII characters --
 * in UTF-32 multibyte characters from UTF-8 are represented by a single
 * entity.
 *
 * @{
 */

enum {
    CVT_NULL  = 0, //!< No conversion was performed and/or NULL character was encountered
    CVT_ERROR = -1 //!< Conversion was not performed due to an error
};

/**
 * @name String conversion
 * @{
 */

/**
 * @brief Convert a UTF-8 string to its UTF-32 form.
 * @param[in] in    Input UTF-8 string (multibyte encoding).
 * @param[out] out  Output UTF-32 string (one character per code unit)
 * @param out_max   Capacity of the output array in bytes.
 * @returns Whether the conversion succeeded or not.
 */
extern bool u8_to_u32(const char *in, char32_t *out, int out_max);

/**
 * @brief Convert a UTF-32 string to its UTF-8 form.
 * @param[in] in    Input UTF-32 string (one character per code unit).
 * @param[out] out  Output UTF-8 string (multibyte encoding)
 * @param out_max   Capacity of the output array in bytes.
 * @returns Whether the conversion succeeded or not.
 */
extern bool u32_to_u8(const char32_t *in, char *out, int out_max);

/**@}*/
/**
 * @name Character conversion
 * @{
 */

/**
 * @brief Decode a single UTF-8 character
 * @param[out] out  Destination UTF-32 character
 * @param[in] in    Input UTF-8 string
 * @param avail     Number of available bytes for reading
 * @returns Number of decoded bytes if decoding was successful and character
 *          was not the null character. CVT_NULL if the character was null character
 *          or if avail == 0. CVT_ERROR on error (errno is set in that case).
 */
extern int u8_dec_char(char32_t *out, const char *in, int avail);

/**
 * @brief Encode a single UTF-8 character
 * @param[out] out   Destination UTF-8 buffer
 * @param[in] in     Input UTF-32 codepoint
 * @param out_avail  Number of available bytes for writing
 * @returns Number of encoded bytes if decoding was successful and character
 *          was not the null character. CVT_NULL if the character was null character.
 *          CVT_ERROR on error (errno is set in that case).
 */
extern int u8_enc_char(char *out, char32_t in, int out_avail);

/**@}*/
/**@}*/

#endif // CORE_UTF8_H
