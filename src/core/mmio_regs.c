/**
 * @file mmio_regs.c
 * @brief memory accessor functions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup mmio
 */

#include "core/mmio_regs.h"

// MMIO access barriers:
// https://wiki.osdev.org/Memory_mapped_registers_in_C/C++

// inspired by:
// - https://elixir.bootlin.com/linux/latest/source/arch/arm/include/asm/io.h
// - https://elixir.bootlin.com/linux/latest/source/arch/arm/include/asm/barrier.h
// - https://developer.arm.com/documentation/ddi0406/c/Application-Level-Architecture/Instruction-Details/Alphabetical-list-of-instructions/DSB?lang=en

/*
 * These memory barriers are most likely an overkill.
 * However, they should ensure that the buffers are always
 * drained and no reordering is allowed to occur.
 */

/**
 * @addtogroup mmio
 * @{
 */

uint8_t mmio_read8(volatile void *base, size_t offset)
{
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    uint8_t value = *((volatile uint8_t *) (((volatile uint8_t *) base) + offset));
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    return value;
}

uint16_t mmio_read16(volatile void *base, size_t offset)
{
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    uint16_t value = *((volatile uint16_t *) (((volatile uint8_t *) base) + offset));
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    return value;
}

uint32_t mmio_read32(volatile void *base, size_t offset)
{
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    uint32_t value = *((volatile uint32_t *) (((volatile uint8_t *) base) + offset));
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    return value;
}


void mmio_write8(volatile void *base, size_t offset,  uint8_t value)
{
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    *((volatile uint8_t *) (((volatile uint8_t *) base) + offset)) = value;
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
}

void mmio_write16(volatile void *base, size_t offset, uint16_t value)
{
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    *((volatile uint16_t *) (((volatile uint8_t *) base) + offset)) = value;
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
}

void mmio_write32(volatile void *base, size_t offset, uint32_t value)
{
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
    *((volatile uint32_t *) (((volatile uint8_t *) base) + offset)) = value;
    __asm__ __volatile__ ("dsb\n\t" ::: "memory");
}

/**@}*/
