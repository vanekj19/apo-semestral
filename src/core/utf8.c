/**
 * @file utf8.c
 * @brief UTF-8 string codec implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup utf8
 */

#include <uchar.h>
#include <stdlib.h>
#include <string.h>
#include "core/utf8.h"
#include "core/logging.h"

bool u8_to_u32(const char *in, char32_t *out, int out_max)
{
    log_assert(out_max > 0, "zero-length output buffer is not allowed");

    const char *p_in  = in;
    char32_t   *p_out = out;
    int in_remain     = strlen(in) + 1;
    int out_remain    = out_max;

    int ret;
    while ((ret = u8_dec_char(p_out, p_in, in_remain)) != CVT_NULL) {
        if (ret > 0) {
            p_in  += ret;
            p_out += 1;
            in_remain  -= ret;
            out_remain -= 1;
        } else {
            log_sys_error("cannot decode multibyte string");
            return false;
        }

        log_assert(in_remain >= 0, "internal logic error - unterminated input string");

        if (out_remain <= 0) {
            log_error("cannot decode multibyte string: input too long");
            return false;
        }
    }

    return true;
}

bool u32_to_u8(const char32_t *in, char *out, int out_max)
{
    log_assert(out_max > 0, "zero-length output buffer is not allowed");

    const char32_t *p_in  = in;
    char           *p_out = out;
    int out_remain        = out_max;

    int ret;
    while ((ret = u8_enc_char(p_out, *p_in, out_remain)) != CVT_NULL) {
        if (ret > 0) {
            p_in  += 1;
            p_out += ret;
            out_remain -= ret;

        } else {
            log_sys_error("cannot encode multibyte string");
            return false;
        }

        if (out_remain <= 0) {
            log_error("cannot encode multibyte string: input too long");
            return false;
        }
    }

    return true;
}
