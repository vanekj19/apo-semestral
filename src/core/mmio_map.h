/**
 * @file mmio_map.h
 * @brief mapping of the physical address to process
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup mmio
 */

#ifndef CORE_MMIO_MAP_H
#define CORE_MMIO_MAP_H

#include <stdbool.h>
#include <sys/types.h>

/**
 * @addtogroup mmio
 * @{
 */

/**
 * @name MMIO mapping/unmapping
 * @{
 */

//! Location of the special memory device
#define MEMORY_DEVICE "/dev/mem"

/**
 * @brief Hardware memory pointer
 */
typedef volatile void mmio_region;

/**
 * @brief Map the given physical address to program's virtual address space.
 * @param physical_address Physical peripheral address.
 * @param region_size      Size of the region to map (bytes, will be rounded up to pages).
 * @param enable_cache     Whether to mark the region as cacheable or not.
 * @returns Mapped peripheral address or NULL.
 */
extern mmio_region *mmio_map(intptr_t physical_address, size_t region_size, bool enable_cache);

/**
 * @brief Unmap the given mapped address from program's virtual address space.
 * @param memory      Virtual memory address to unmap.
 * @param region_size Length of the area to unmap.
 */
extern void mmio_unmap(mmio_region *memory, size_t region_size);

/**@}*/
/**@}*/

#endif // CORE_MMIO_MAP_H
