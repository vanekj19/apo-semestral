/**
 * @defgroup core Core/utility module
 * @brief Collection of shared functionalities
 *
 * This module contains various generic or utility functionalities.
 * The features provided are either used in multiple upper-level modules or
 * are utility-like at their core.
 */
