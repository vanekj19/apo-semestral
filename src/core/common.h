/**
 * @file common.h
 * @brief timing/numeric utility functions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup common
 */

#ifndef CORE_COMMON_H
#define CORE_COMMON_H

#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <signal.h>
#include <stdatomic.h>


/**
 * @defgroup common Numeric/timing utilities
 * @ingroup core
 * @brief This module provides a few utility-like functions.
 * @{
 */

/**
 * @name Numeric utilities
 * @{
 */

/**
 * @brief Clip a value to a specific range.
 * @param value Value to clip.
 * @param min   Minimum acceptable value.
 * @param max   Maximum acceptable value.
 * @returns Clipped value.
 */
extern int clip(int value, int min, int max);

/**
 * @brief Clip a value to a specific range.
 * @param value Value to clip.
 * @param min   Minimum acceptable value.
 * @param max   Maximum acceptable value.
 * @returns Clipped value.
 */
extern float clipf(float value, float min, float max);

/**@}*/
/**
 * @name Timing utilities
 * @{
 */

/**
 * @brief Wait for a given amount of time.
 * @param msec Number of milliseconds to wait.
 */
extern void delay_ms(int msec);

/**
 * @brief Wait for next iteration start (loop timing helper).
 * @param last_iter Last iteration timestamp (CLOCK_MONOTONIC).
 * @param frequency Iteration frequency in Hz.
 */
extern void loop_delay(struct timespec *last_iter, int frequency);

/**
 * @brief Get number of milliseconds elapsed since some moment.
 * @param start Starting time point (CLOCK_MONOTONIC).
 * @remarks This method does not support looking into the past
 *          (e.g. negative return values are not supported).
 * @returns Number of milliseconds since that time.
 */
extern long millis_from(struct timespec *start);

/**
 * @brief Compare two timestamps (CLOCK_MONOTONIC).
 * @param a First timestamp to compare.
 * @param b Second timestamp to compare.
 * @returns Value <0 if a is lesser, value >0 if b is lesser, value =0 if a=b.
 */
extern int time_compare(struct timespec *a, struct timespec *b);

/**
 * @brief Query the current monotonic time (CLOCK_MONOTONIC).
 * @returns Current monotonic timestamp.
 */
extern struct timespec now_monotonic(void);

/**@}*/
/**@}*/

#endif
