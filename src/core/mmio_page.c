/**
 * @file mmio_page.c
 * @brief page alignment arithmetic implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup mmio
 */

#include <unistd.h>
#include "core/mmio_page.h"

/**
 * @addtogroup mmio
 * @{
 */

intptr_t round_page_up(intptr_t addr, unsigned long page_size)
{
    return round_page_down(addr + page_size - 1, page_size);
}

intptr_t round_page_down(intptr_t addr, unsigned long page_size)
{
    return addr & ~(page_size - 1);
}

void round_region(intptr_t phy_start, size_t phy_len,
                  intptr_t *p_virt_start, size_t *p_virt_len)
{
    size_t pagesize = sysconf(_SC_PAGESIZE);

    intptr_t region_start = round_page_down(phy_start, pagesize);
    intptr_t region_end   = round_page_up(phy_start + phy_len, pagesize);
    size_t mapping_len    = region_end - region_start;

    *p_virt_start = region_start;
    *p_virt_len   = mapping_len;
}

/**
 * @}
 */
