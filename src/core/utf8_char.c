/**
 * @file utf8_char.c
 * @brief UTF-8 character codec implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup utf8
 */

#include "core/utf8.h"
#include "core/logging.h"
#include <errno.h>
#include <uchar.h>

/**
 * @brief Check if a condition holds; if not, set errno and return -1
 * @param cond  Condition to check
 * @param error errno value to set if the condition doesn't hold
 */
#define assert_or_fail(cond, error) do { if (!(cond)) { errno = (error); return CVT_ERROR; } } while(0)

/**
 * @brief Forward function call failure (return on error)
 * @param expr Expression to check (fail == returned < 0)
 */
#define assert_not_failed(expr) do { if ((expr) < 0) return CVT_ERROR; } while(0)

////////////////////////////////////////////////////////////////
// Encoder
//

/**
 * @brief Determine how many bytes bytes this codepoint occupies in UTF-8.
 * @param codepoint UTF-32 codepoint to scan
 * @returns Number of bytes or -1 if the character is invalid.
 */
static int determine_enc_len(char32_t codepoint);

int u8_enc_char(char *out, char32_t in, int out_avail)
{
    int len = determine_enc_len(in);
    assert_or_fail(len > 0, EILSEQ); // out-of-bounds character
    assert_or_fail(out_avail >= len, EOVERFLOW);

    if (len == 1) {
        out[0] = in & 0x7f;
    } else if (len == 2) {
        out[0] = 0xC0 | ((in >>  6) & 0x1f);
        out[1] = 0x80 | ((in >>  0) & 0x3f);
    } else if (len == 3) {
        out[0] = 0xE0 | ((in >> 12) & 0x0f);
        out[1] = 0x80 | ((in >>  6) & 0x3f);
        out[2] = 0x80 | ((in >>  0) & 0x3f);
    } else if (len == 4) {
        out[0] = 0xF0 | ((in >> 18) & 0x07);
        out[1] = 0x80 | ((in >> 12) & 0x3f);
        out[2] = 0x80 | ((in >>  6) & 0x3f);
        out[3] = 0x80 | ((in >>  0) & 0x3f);
    }

    if (in == 0) {
        return CVT_NULL; // null terminator handling similar to c32tombr
    } else {
        return len;
    }
}

int determine_enc_len(char32_t codepoint)
{
    if (codepoint <= 0x7f) {
        return 1;
    } else if (codepoint <= 0x07FF) {
        return 2;
    } else if (codepoint <= 0xFFFF) {
        return 3;
    } else if (codepoint <= 0x10FFFF) {
        return 4;
    } else {
        return CVT_ERROR;
    }
}


////////////////////////////////////////////////////////////////
// Decoder
//

/**
 * @brief Determine how many bytes does a UTF-8 code point have.
 * @param char0 First byte of the character.
 * @returns Number of bytes or -1 if the character is invalid.
 */
static int determine_dec_len(char char0);

/**
 * @brief Process the data bits of the first codepoint byte.
 * @param ch          Header byte to process.
 * @param valid_bits  How many data bits are there in the first byte.
 * @param[in,out] cp  UTF-32 codepoint to be updated with the bits.
 */
static void eat_header_byte(char ch, int valid_bits, char32_t *cp);

/**
 * @brief Process the data bits of followup carrier bytes.
 * @param ch          Carrier byte to process.
 * @param[in,out] cp  UTF-32 codepoint to be updated with the bits.
 * @returns           0 on success, -1 on error.
 */
static int eat_carrier_byte(char ch, char32_t *cp);

int u8_dec_char(char32_t *out, const char *in, int avail)
{
    *out = 0;
    if (avail <= 0) {
        return CVT_NULL;
    }

    int len = determine_dec_len(in[0]);
    assert_not_failed(len);
    assert_or_fail(avail >= len, EILSEQ);

    if (len == 1) {
        *out = in[0];
    } else {
        eat_header_byte(in[0], 7 - len, out);
        for (int i = 1; i < len; i++) {
            assert_not_failed(eat_carrier_byte(in[i], out));
        }
    }

    int enc_len = determine_enc_len(*out);
    assert_or_fail(len == enc_len, EILSEQ); // overlong aliases are not permitted

    if (len == 1 && *out == 0) {
        return CVT_NULL; // null terminator handling similar to mbrtoc32
    } else {
        return len;
    }
}

void eat_header_byte(char ch, int valid_bits, char32_t *cp)
{
    uint8_t mask = (1u << valid_bits) - 1;
    *cp = (*cp << valid_bits) | (ch & mask);
}

int eat_carrier_byte(char ch, char32_t *cp)
{
    assert_or_fail((ch & 0xC0) == 0x80, EILSEQ);
    *cp = (*cp << 6) | (ch & 0x3f);
    return 0;
}

int determine_dec_len(char char0)
{
    if ((char0 & 0x80) == 0x00) {
        return 1; // plain old ASCII
    } else if ((char0 & 0xE0) == 0xC0) {
        return 2;
    } else if ((char0 & 0xF0) == 0xE0) {
        return 3;
    } else if ((char0 & 0xF8) == 0xF0) {
        return 4;
    } else {
        return CVT_ERROR;
    }
}
