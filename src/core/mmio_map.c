/**
 * @file mmio_map.c
 * @brief mapping of the physical address to process
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup mmio
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "core/logging.h"
#include "core/mmio_map.h"
#include "core/mmio_page.h"


/**
 * @addtogroup mmio
 * @{
 */


////////////////////////////////////////////////////////////////
// Hardware memory mapping
//

mmio_region *mmio_map(intptr_t physical_address, size_t region_size, bool enable_cache)
{
    log_debug("mmapping address %#08zx with length %#08zx", physical_address, region_size);
    intptr_t vma_phy_start = 0;
    size_t   vma_len = 0;

    round_region(physical_address, region_size, &vma_phy_start, &vma_len);
    size_t moved_back_by = physical_address - vma_phy_start;

    int fd = open(MEMORY_DEVICE, O_RDWR | (!enable_cache ? O_SYNC : 0));
    if (fd < 0) {
        log_sys_error("cannot open %s to map a peripheral", MEMORY_DEVICE);
        return NULL;
    }

    void *mem = mmap(NULL, vma_len,
                     PROT_READ | PROT_WRITE, MAP_SHARED,
                     fd, vma_phy_start);

    mmio_region *result = NULL;
    if (mem == MAP_FAILED) {
        log_sys_error("cannot map peripheral at physaddr %#08zx len %08zu",
                      physical_address, region_size);
    } else {
        result = (mmio_region*) ((intptr_t) mem + moved_back_by);
    }

    close(fd);
    return result;
}


////////////////////////////////////////////////////////////////
// Hardware memory unmapping
//

void mmio_unmap(mmio_region *memory, size_t region_size)
{
    log_debug("unmapping virtual address %#08zx", (intptr_t) memory);
    intptr_t virt_start;
    size_t   virt_len;

    round_region((intptr_t) memory, region_size, &virt_start, &virt_len);

    if (munmap((void*) virt_start, virt_len) < 0) {
        log_sys_warn("cannot unmap peripheral");
    }
}

/**@}*/
