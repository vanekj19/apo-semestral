/**
 * @file mmio_page.h
 * @brief helpers for page alignment arithmetic
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup mmio
 */

#ifndef CORE_MMIO_PAGE_H
#define CORE_MMIO_PAGE_H

#include <stdint.h>
#include <stdlib.h>

/**
 * @addtogroup mmio
 * @{
 */

/** @name Page alignment helpers
 * @{
 */

/**
 * @brief Expand a region to start and end on system memory page boundaries.
 * @param phy_start    Start of the requested region.
 * @param phy_len      Length of the requested region.
 * @param p_vma_start  Page-aligned start of the region.
 * @param p_vma_len    Page-aligned length of the region.
 */
extern void round_region(intptr_t phy_start, size_t phy_len,
                         intptr_t *p_vma_start, size_t *p_vma_len);

/**
 * @brief Round an address up to a memory page boundary.
 * @param addr       Address to round up.
 * @param page_size  System page size.
 * @returns          Rounded-up addr.
 */
extern intptr_t round_page_up(intptr_t addr, unsigned long page_size);

/**
 * @brief Round an address down to a memory page boundary.
 * @param addr       Address to round down.
 * @param page_size  System page size.
 * @returns          Rounded-down addr.
 */
extern intptr_t round_page_down(intptr_t addr, unsigned long page_size);

/**@}*/
/**@}*/

#endif // CORE_MMIO_PAGE_H
