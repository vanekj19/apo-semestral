/**
 * @file drawing_lines.c
 * @brief Horizontal/vertical line renderer
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_ll
 */

#include "render/drawing.h"


void draw_horizontal_line(lcd_frame *frame, int x0, int y0, int length, rgb565 color)
{
    for (int x = x0; x < x0 + length; x++) {
        pixel_set(frame, x, y0, color);
    }
}

void draw_vertical_line(lcd_frame *frame, int x0, int y0, int length, rgb565 color)
{
    for (int y = y0; y < y0 + length; y++) {
        pixel_set(frame, x0, y, color);
    }
}
