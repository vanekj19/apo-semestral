/**
 * @file render_top.c
 * @brief Top statusbar rendering logic
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_hl
 */

#include "fonts/font_types.h"
#include "core/logging.h"
#include "config/config.h"
#include "render/render.h"
#include "render/render_flows.h"
#include "render/drawing.h"
#include "render/render_config.h"

//! Statusbar font style
static text_style topbar_style = {
    .scale   = TOPBAR_SCALE,
    .spacing = TOPBAR_SPACING,
    .color   = TOPBAR_FGCOLOR,
    .font    = TOPBAR_FONT
};

void render_top_bar(lcd_frame *frame, screen_state *state)
{
    draw_filled_rect(frame, 0, 0, LCD_WIDTH, TOPBAR_HEIGHT, TOPBAR_BGCOLOR);
    render_lock_name(frame, state);
    render_current_time(frame, state);
}

void render_lock_name(lcd_frame *frame, screen_state *state)
{
    draw_text_line(frame, TOPBAR_PAD_SIDE, TOPBAR_PAD_TOP,
                   CONFIG.lock_name, &topbar_style);
}

void render_current_time(lcd_frame *frame, screen_state *state)
{
    char32_t date[DATE_PLACES];
    date[0] = U'0' + state->clock_time.tm_hour / 10;
    date[1] = U'0' + state->clock_time.tm_hour % 10;
    date[2] = U':';
    date[3] = U'0' + state->clock_time.tm_min / 10;
    date[4] = U'0' + state->clock_time.tm_min % 10;
    date[5] = U':';
    date[6] = U'0' + state->clock_time.tm_sec / 10;
    date[7] = U'0' + state->clock_time.tm_sec % 10;
    date[8] = U'\0';

    draw_text_line_adj(frame, 0, TOPBAR_PAD_TOP, LCD_WIDTH - TOPBAR_PAD_SIDE,
                       TEXT_RIGHT, date, &topbar_style);
}
