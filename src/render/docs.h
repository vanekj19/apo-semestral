/**
 * @defgroup render Screen renderer module
 * @brief Screen rendering logic
 *
 * This module provides a high-level screen rendering API to the application.
 * Only a single struct with necessary information is needed; all other
 * operations are abstracted away from the application.
 */

/**
 * @defgroup render_hl LCD screen renderer
 * @brief High-level rendering code
 * @ingroup render
 */

/**
 * @defgroup render_ll Drawing utilities
 * @brief Low-level shape drawing functions
 * @ingroup render
 */
