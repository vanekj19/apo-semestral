/**
 * @file drawing_img.c
 * @brief XBM image drawer
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_ll
 */

#include "render/drawing.h"

//! Check if a pixel is black in a XBM image
static bool xbm_pixel_set(const image *img, int x, int y);

//! Draw a scaled-up pixel onto the screen
static void blit_pixel(lcd_frame *frame, int x, int y, int ix, int iy,
                       const image *img);

void draw_image(lcd_frame *frame, int x, int y, const image *img)
{
    for (int iy = 0; iy < img->height; iy++) {
        for (int ix = 0; ix < img->width; ix++) {
            if (xbm_pixel_set(img, ix, iy)) {
                blit_pixel(frame, x, y, ix, iy, img);
            }
        }
    }
}

bool xbm_pixel_set(const image *img, int x, int y)
{
    int stride = (img->width + 7) / 8;
    int byte_idx = stride * y + x / 8;
    int bit_idx  = x % 8;

    uint8_t bit_mask = 1u << bit_idx;
    return (img->pixels[byte_idx] & bit_mask) != 0;
}

void blit_pixel(lcd_frame *frame, int x, int y, int ix, int iy, const image *img)
{
    int x0 = x + ix * img->scale;
    int y0 = y + iy * img->scale;

    draw_filled_rect(frame, x0, y0, img->scale, img->scale, img->color);
}
