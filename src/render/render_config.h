/**
 * @file render_config.h
 * @brief Macro-based UI configuration
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_hl
 */

#ifndef LCD_RENDER_CONFIG_H
#define LCD_RENDER_CONFIG_H

/**
 * @addtogroup render_hl
 * @{
 */

#define RGB3(r,g,b) (pixel_pack((rgbf){(r), (g), (b)})) //!< Helper macro for packing RGB565 pixels
#define RGB1(i)     (pixel_pack((rgbf){(i), (i), (i)})) //!< Helper macro for packing gray RGB565 pixels

#define RGB565_WHITE 0xFFFF //!< RGB565 white color
#define RGB565_BLACK 0x0000 //!< RGB565 black color
#define DATE_PLACES 9       //!< Number of characters in the date string ("00:00:00")

#define MAIN_BGINTENSITY 0.8f                   //!< Background color intensity
#define MAIN_BGCOLOR     RGB1(MAIN_BGINTENSITY) //!< Background color

/** @name Top statusbar parameters
 * @{
 */
#define TOPBAR_HEIGHT   40
#define TOPBAR_PAD_TOP   4
#define TOPBAR_PAD_SIDE 10
#define TOPBAR_FONT      font_wVerdana_16
#define TOPBAR_SCALE     2
#define TOPBAR_SPACING   0
#define TOPBAR_BGCOLOR   RGB565_BLACK
#define TOPBAR_FGCOLOR   RGB565_WHITE
/**@}*/

/** @name Top statusbar text parameters
 * @{
 */
#define STATUSTEXT_SCALE     2
#define STATUSTEXT_SPACING   0
#define STATUSTEXT_FONT      font_wTahoma_40
#define STATUSTEXT_POSITION     65
#define STATUSTEXT_POSITION_IMG 69
#define STATUSTEXT_COLOR     RGB565_BLACK
#define STATUSTEXT_IMG_PAD   10
#define STATUSTEXT_RED       0xB000
/**@}*/

/** @name Pin digit parameters
 * @{
 */
#define PIN_SCALE       5
#define PIN_FONT        font_rom8x16
#define PIN_WIDTH       (LCD_WIDTH /2)
#define PIN_POSITION    180
#define PIN_COLOR       RGB565_BLACK
/**@}*/

/** @name Pin digit box parameters
 * @{
 */
#define PIN_BBOX_PAD_TOP   3
#define PIN_BBOX_PAD_SIDE 10
#define PIN_BBOX_W        55
#define PIN_BBOX_H        76
#define PIN_BBOX_PIXS      3
#define PIN_BBOX_COLOR     RGB565_BLACK
#define PIN_BBOX_BGCOLOR   RGB3(0.8f, 0.8f, 0.8f)
/**@}*/

/** @name Pin "status stripe" parameters
 * @{
 */
#define PIN_MEGABOX_WIDTH    (LCD_WIDTH + 10)
#define PIN_MEGABOX_HEIGHT   100
#define PIN_MEGABOX_POSITION 165
#define PIN_MEGABOX_BORDER   3
#define PIN_MEGABOX_BORDER_COLOR    RGB565_BLACK
#define PIN_MEGABOX_COLOR_LOCKED    RGB3(0.0f, 0.0f, 1.0f)
#define PIN_MEGABOX_COLOR_UNLOCKED  RGB3(0.0f, 0.8f, 0.0f)
#define PIN_MEGABOX_COLOR_INCORRECT RGB3(1.0f, 0.0f, 0.0f)
/**@}*/

/** @name Bottom navigation bar parameters
 * @{
 */#define BOTBAR_HEIGHT      36
#define BOTBAR_PAD_BOTTOM  3
#define BOTBAR_SCALE       2
#define BOTBAR_SPACING     1
#define BOTBAR_FONT        font_rom8x16
#define BOTBAR_BORDER      2
#define BOTBAR_BORDER_COLOR RGB565_BLACK
#define BOTBAR_COLOR_A     RGB3(0.8f, 0.4f, 0.5f)
#define BOTBAR_COLOR_B     RGB3(0.5f, 0.7f, 0.5f)
#define BOTBAR_COLOR_C     RGB3(0.5f, 0.4f, 0.9f)
#define BOTBAR_TEXT_COLOR  RGB565_BLACK
/**@}*/


/**@}*/
#endif // LCD_RENDER_CONFIG_H
