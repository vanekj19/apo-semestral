/**
 * @file render_flows.h
 * @brief High-level screen rendering function declarations
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_hl
 */

#ifndef LCD_RENDER_H
#define LCD_RENDER_H

#include "render/render.h"

/**
 * @addtogroup render_hl
 * @{
 */

/**
 * @brief Render the screen background.
 * @param frame Output frame bitmap
 */
extern void render_bg(lcd_frame *frame);

/**
 * @brief Render the top statusbar.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_top_bar(lcd_frame *frame, screen_state *state);

/**
 * @brief Render the lock name into the top statusbar.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_lock_name(lcd_frame *frame, screen_state *state);

/**
 * @brief Render the current time into the top statusbar.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_current_time(lcd_frame *frame, screen_state *state);

/**
 * @brief Render the lock state text.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_lock_state(lcd_frame *frame, screen_state *state);

/**
 * @brief Render all the graphics related to PIN digits.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_pin(lcd_frame *frame, screen_state *state);

/**
 * @brief Render the big status stripe behind the PIN digits.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_pin_megabox(lcd_frame *frame, screen_state *state);

/**
 * @brief Render one PIN digit with its surrounding box.
 * @param frame   Output frame bitmap
 * @param digit   Digit to render into the box.
 * @param char_x0 Top-left point X coordinate of the *character* bounding box.
 * @param char_y0 Top-left point Y coordinate of the *character* bounding box.
 */
extern void render_pin_digit(lcd_frame *frame, int digit, int char_x0, int char_y0);

/**
 * @brief Render the bottom navigation bar.
 * @param frame Output frame bitmap
 * @param state LCD rendering context for details about contents
 */
extern void render_bottom_bar(lcd_frame *frame, screen_state *state);

/**
 * @brief Render a segment of the bottom navigation bar.
 * @param frame Output frame bitmap
 * @param x0    Top-left point X coordinate of the segment
 * @param y0    Top-left point Y coordinate of the segment
 * @param w     Width of the segment in pixels
 * @param color Background color of the segment
 * @param text  Navigation text to display within the segment
 */
extern void render_bottom_bar_cell(lcd_frame *frame, int x0, int y0, int w,
                                   rgb565 color, const char32_t *text);

/**@}*/
#endif // LCD_RENDER_H
