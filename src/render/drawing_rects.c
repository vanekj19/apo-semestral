/**
 * @file drawing_rects.c
 * @brief Filled & hollow rectangle renderer
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_ll
 */

#include "render/drawing.h"


void draw_clear(lcd_frame *frame, rgb565 color)
{
    for (int y = 0; y < LCD_HEIGHT; y++) {
        for (int x = 0; x < LCD_WIDTH; x++) {
            frame->pixels[y][x] = color;
        }
    }
}

void draw_filled_rect(lcd_frame *frame, int x0, int y0, int w, int h, rgb565 color)
{
    int xend = x0 + w;
    int yend = y0 + h;
    for (int y = y0; y < yend; y++) {
        for (int x = x0; x < xend; x++) {
            pixel_set(frame, x, y, color);
        }
    }
}

void draw_hollow_rect(lcd_frame *frame, int x0, int y0, int w, int h, int thickness, rgb565 color)
{
    //                      corner_left         corner_top          width      height             color
    draw_filled_rect(frame, x0,                 y0,                 w,         thickness,         color); // top
    draw_filled_rect(frame, x0,                 y0 + h - thickness, w,         thickness,         color); // bottom
    draw_filled_rect(frame, x0,                 y0 + thickness,     thickness, h - 2 * thickness, color); // left
    draw_filled_rect(frame, x0 + w - thickness, y0 + thickness,     thickness, h - 2 * thickness, color); // right
}
