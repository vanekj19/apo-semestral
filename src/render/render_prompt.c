/**
 * @file render_prompt.c
 * @brief Middle status text rendering
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_hl
 */

#include "fonts/font_types.h"
#include "core/logging.h"
#include "config/config.h"
#include "render/render.h"
#include "render/render_flows.h"
#include "render/drawing.h"
#include "render/render_config.h"
#include "render/locked.xbm"
#include "render/unlocked.xbm"
#include "render/incorrect.xbm"

//! Get the line of text that should be drawn in the middle
static const char32_t *get_status_text(ui_state state);

//! Get the icon that should be drawn in the middle
static const image *get_status_icon(ui_state state);

//! Main status text style
static text_style status_style = {
    .scale   = STATUSTEXT_SCALE,
    .spacing = STATUSTEXT_SPACING,
    .color   = STATUSTEXT_COLOR,
    .font    = STATUSTEXT_FONT
};

//! Locked lock icon
static image icon_locked = {
    .width  = locked_width,
    .height = locked_height,
    .pixels = locked_bits,
    .color  = STATUSTEXT_COLOR,
    .scale  = 2
};

//! Unlocked lock icon
static image icon_unlocked = {
    .width  = unlocked_width,
    .height = unlocked_height,
    .pixels = unlocked_bits,
    .color  = STATUSTEXT_COLOR,
    .scale  = 2
};

//! Incorrect code entered icon
static image icon_error = {
    .width  = incorrect_width,
    .height = incorrect_height,
    .pixels = incorrect_bits,
    .color  = STATUSTEXT_RED,
    .scale  = 1
};


void render_lock_state(lcd_frame *frame, screen_state *state)
{
    const char32_t *str = get_status_text(state->lock_status);
    const image    *img = get_status_icon(state->lock_status);

    int img_w  = img->width * img->scale;
    int text_w = draw_get_text_len(str, &status_style);

    int img_x0 = (LCD_WIDTH - text_w - img_w - STATUSTEXT_IMG_PAD) / 2;
    int img_y0 = STATUSTEXT_POSITION_IMG;
    int text_x0 = img_x0 + img_w + STATUSTEXT_IMG_PAD;
    int text_y0 = STATUSTEXT_POSITION;

    draw_image(frame, img_x0, img_y0, img);
    draw_text_line(frame, text_x0, text_y0, str, &status_style);
}

const char32_t *get_status_text(ui_state state)
{
    const char32_t *infotext;
    switch (state) {
    case LOCK_LOCKED:         infotext = CONFIG.locked_text;         break;
    case LOCK_UNLOCKED:       infotext = CONFIG.unlocked_text;       break;
    case LOCK_UNLOCK_FAILED:  infotext = CONFIG.incorrect_code_text; break;
    default:                  infotext = U"PROGRAM BUG";             break;
    }
    return infotext;
}

const image *get_status_icon(ui_state state)
{
    const image *icon;
    switch (state) {
    case LOCK_LOCKED:         icon = &icon_locked;   break;
    case LOCK_UNLOCKED:       icon = &icon_unlocked; break;
    default:                  icon = &icon_error;    break;
    }
    return icon;
}
