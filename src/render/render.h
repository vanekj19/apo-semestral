/**
 * @file render.h
 * @brief Public API of the renderer module
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render
 */

#ifndef RENDER_INTERFACE_H
#define RENDER_INTERFACE_H

#include "core/common.h"
#include "core/mmio_regs.h"
#include "core/defs.h"

/**
 * @addtogroup render
 * @{
 */

//! Complete description of the screen state.
typedef struct {
    int       code;        //!< Displayed lock code
    ui_state  lock_status; //!< UI type (locked/unlocked)
    struct tm clock_time;  //!< Current wall clock time
} screen_state;

/**
 * @brief Render the specified state onto a canvas.
 * @param frame Pixel buffer into which to render the image.
 * @param state Display state to render.
 */
extern void render_screen(lcd_frame *frame, screen_state *state);

/**@}*/
#endif // RENDER_INTERFACE_H
