/**
 * @file render.c
 * @brief Top-level screen renderer flow
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render
 */

#include "fonts/font_types.h"
#include "core/logging.h"
#include "config/config.h"
#include "render/render.h"
#include "render/render_flows.h"
#include "render/drawing.h"
#include "render/render_config.h"

void render_screen(lcd_frame *frame, screen_state *state)
{
    render_bg(frame);
    render_top_bar(frame, state);
    render_lock_state(frame, state);
    render_pin(frame, state);
    render_bottom_bar(frame, state);
}

void render_bg(lcd_frame *frame)
{
    draw_clear(frame, MAIN_BGCOLOR);
}
