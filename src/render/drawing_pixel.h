/**
 * @file drawing_pixel.h
 * @brief Pixel conversion & access wrappers
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_ll
 */

#include "render/drawing.h"
#include "core/common.h"

void pixel_set(lcd_frame *canvas, int x, int y, rgb565 pixel)
{
    if (x < 0          || y < 0)           return;
    if (x >= LCD_WIDTH || y >= LCD_HEIGHT) return;
    canvas->pixels[y][x] = pixel;
}

rgb565 pixel_get(lcd_frame *canvas, int x, int y)
{
    if (x < 0          || y < 0)           return 0;
    if (x >= LCD_WIDTH || y >= LCD_HEIGHT) return 0;
    return canvas->pixels[y][x];
}

rgb565 pixel_pack(rgbf wide)
{
    uint16_t red   = clip(wide.red    * 0x1f, 0x00, 0x1f);
    uint16_t green = clip(wide.green  * 0x3f, 0x00, 0x3f);
    uint16_t blue  = clip(wide.blue   * 0x1f, 0x00, 0x1f);

    return (red << 11) | (green << 5) | (blue << 0);
}

rgbf pixel_unpack(rgb565 narrow)
{
    uint16_t red   = (narrow >> 11) & 0x1f;
    uint16_t green = (narrow >>  5) & 0x3f;
    uint16_t blue  = (narrow >>  0) & 0x1f;
    return (rgbf) {
        .red   = red   / 31.0f,
        .green = green / 63.0f,
        .blue  = blue  / 31.0f,
    };
}
