/**
 * @file drawing_font.c
 * @brief Font renderer
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_ll
 */

#include "render/drawing.h"
#include "fonts/glyphs.h"
#include "core/logging.h"

//! Draw an on-screen scaled pixel.
static void set_font_pix(lcd_frame *frame, int x, int y, int cx, int cy, int scale, rgb565 color);

//! Blit a glyph onto the specified position on the canvas
static void internal_draw(lcd_frame *frame, int x, int y, int scale, rgb565 color, const glyph_info *glyph);

void draw_text_line_adj(lcd_frame *frame, int x, int y, int w, text_adj adj,
                        const char32_t *text, const text_style *style)
{
    int text_w = draw_get_text_len(text, style);

    int new_x0;
    if (adj == TEXT_LEFT) {
        new_x0 = x;
    } else if (adj == TEXT_CENTER) {
        new_x0 = x + (w - text_w) / 2;
    } else if (adj == TEXT_RIGHT) {
        new_x0 = x + w - text_w;
    } else {
        log_unreachable("invalid justification mode provided");
    }

    draw_text_line(frame, new_x0, y, text, style);
}

void draw_text_line(lcd_frame *frame, int x, int y, const char32_t *line,
                    const text_style *style)
{
    glyph_info glyph;
    for (; *line != U'\0'; line++) {
        glyph_lookup(*line, style->font, &glyph);
        internal_draw(frame, x, y, style->scale, style->color, &glyph);
        x += (glyph.width + style->spacing) * style->scale;
    }
}


unsigned draw_get_text_len(const char32_t *line, const text_style *style)
{
    glyph_info glyph;
    unsigned len = 0;

    for (; *line != U'\0'; line++) {
        glyph_lookup(*line, style->font, &glyph);
        len += (glyph.width + style->spacing) * style->scale;
    }

    if (len > 0) {
        len -= style->spacing * style->scale;
    }

    return len;
}

void draw_text_char(lcd_frame *frame, int x, int y,
                    char32_t letter, const text_style *style)
{
    glyph_info glyph;
    glyph_lookup(letter, style->font, &glyph);
    internal_draw(frame, x, y, style->scale, style->color, &glyph);
}


void internal_draw(lcd_frame *frame, int x, int y, int scale,
                   rgb565 color, const glyph_info *glyph)
{
    for (unsigned cy = 0; cy < glyph->height; cy++) {
        for (unsigned cx = 0; cx < glyph->width; cx++) {
            if (glyph_pixel_active(cx, cy, glyph)) {
                set_font_pix(frame, x, y, cx, cy, scale, color);
            }
        }
    }
}

void set_font_pix(lcd_frame *frame, int x, int y, int cx, int cy, int scale, rgb565 color)
{
    int x0 = x + cx * scale;
    int y0 = y + cy * scale;

    for (int dy = 0; dy < scale; dy++) {
        for (int dx = 0; dx < scale; dx++) {
            pixel_set(frame, x0 + dx, y0 + dy, color);
        }
    }
}
