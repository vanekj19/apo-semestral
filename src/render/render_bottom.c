/**
 * @file render_bottom.c
 * @brief Bottom navigation bar rendering logic
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_hl
 */

#include "fonts/font_types.h"
#include "core/logging.h"
#include "config/config.h"
#include "render/render.h"
#include "render/render_flows.h"
#include "render/drawing.h"
#include "render/render_config.h"

//! Bottom navbar font style
static text_style botbar_style = {
    .scale   = BOTBAR_SCALE,
    .spacing = BOTBAR_SPACING,
    .color   = BOTBAR_TEXT_COLOR,
    .font    = BOTBAR_FONT
};

void render_bottom_bar(lcd_frame *frame, screen_state *state)
{
    int ref_y0 = LCD_HEIGHT - BOTBAR_HEIGHT - 1;
    int border_y0 = ref_y0 - BOTBAR_BORDER;

    draw_filled_rect(frame, 0, border_y0, LCD_WIDTH, BOTBAR_BORDER, BOTBAR_BORDER_COLOR);

    int x0_step = LCD_WIDTH / 3;
    int x0_a = 0;
    int x0_b = x0_step;
    int x0_c = LCD_WIDTH - x0_step;
    int x0_d = LCD_WIDTH;

    const char32_t *moveonly = U"\x1e\x1f";
    const char32_t *infotext = state->lock_status == LOCK_UNLOCKED ? U"\x1e\x1f / X" : U"\x1e\x1f / OK";

    render_bottom_bar_cell(frame, x0_a, ref_y0, x0_b - x0_a, BOTBAR_COLOR_A, moveonly);
    render_bottom_bar_cell(frame, x0_b, ref_y0, x0_c - x0_b, BOTBAR_COLOR_B, moveonly);
    render_bottom_bar_cell(frame, x0_c, ref_y0, x0_d - x0_c, BOTBAR_COLOR_C, infotext);
}

void render_bottom_bar_cell(lcd_frame *frame, int x0, int y0, int w,
                            rgb565 color, const char32_t *text)
{
    draw_filled_rect(frame, x0, y0, w, BOTBAR_HEIGHT, color);

    draw_text_line_adj(frame, x0, y0 + BOTBAR_PAD_BOTTOM, w, TEXT_CENTER,
                       text, &botbar_style);
}
