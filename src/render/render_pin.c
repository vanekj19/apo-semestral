/**
 * @file render_pin.c
 * @brief PIN code rendering
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_hl
 */

#include "fonts/font_types.h"
#include "core/logging.h"
#include "config/config.h"
#include "render/render.h"
#include "render/render_flows.h"
#include "render/drawing.h"
#include "render/render_config.h"

//! Pin digit style
static text_style pin_style = {
    .scale   = PIN_SCALE,
    .spacing = 0,
    .color   = PIN_COLOR,
    .font    = PIN_FONT
};

void render_pin(lcd_frame *frame, screen_state *state)
{
    render_pin_megabox(frame, state);
    int digit_width  = PIN_FONT[0]->maxwidth * PIN_SCALE;

    int interdigit = (PIN_WIDTH - 3 * digit_width) / 2;
    int x0_step = digit_width + interdigit;
    int x0_a = (LCD_WIDTH - PIN_WIDTH) / 2;
    int x0_b = x0_a + x0_step;
    int x0_c = x0_b + x0_step;
    int y0   = PIN_POSITION;

    int code = state->code;
    render_pin_digit(frame, (code / 100) % 10, x0_a, y0);
    render_pin_digit(frame, (code /  10) % 10, x0_b, y0);
    render_pin_digit(frame, (code /   1) % 10, x0_c, y0);
}

void render_pin_megabox(lcd_frame *frame, screen_state *state)
{
    rgb565 bgcolor;
    switch (state->lock_status) {
    case LOCK_LOCKED:         bgcolor = PIN_MEGABOX_COLOR_LOCKED;    break;
    case LOCK_UNLOCKED:       bgcolor = PIN_MEGABOX_COLOR_UNLOCKED;  break;
    case LOCK_UNLOCK_FAILED:  bgcolor = PIN_MEGABOX_COLOR_INCORRECT; break;
    default:                  bgcolor = RGB565_WHITE;                break;
    }

    int x0 = (LCD_WIDTH - PIN_MEGABOX_WIDTH) / 2;

    draw_filled_rect(frame, x0, PIN_MEGABOX_POSITION,
                     PIN_MEGABOX_WIDTH, PIN_MEGABOX_HEIGHT,
                     bgcolor);
    draw_hollow_rect(frame, x0, PIN_MEGABOX_POSITION,
                     PIN_MEGABOX_WIDTH, PIN_MEGABOX_HEIGHT,
                     PIN_MEGABOX_BORDER, PIN_MEGABOX_BORDER_COLOR);
}


void render_pin_digit(lcd_frame *frame, int digit, int char_x0, int char_y0)
{
    draw_filled_rect(frame,
        char_x0 - PIN_BBOX_PAD_SIDE, char_y0 - PIN_BBOX_PAD_TOP,
        PIN_BBOX_W, PIN_BBOX_H, PIN_BBOX_BGCOLOR);

    draw_hollow_rect(frame,
        char_x0 - PIN_BBOX_PAD_SIDE, char_y0 - PIN_BBOX_PAD_TOP,
        PIN_BBOX_W, PIN_BBOX_H,
        PIN_BBOX_PIXS, PIN_BBOX_COLOR);

    draw_text_char(frame, char_x0, char_y0, U'0' + digit, &pin_style);
}
