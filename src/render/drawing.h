/**
 * @file drawing.h
 * @brief Interface for low-level rendering functions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup render_ll
 */

#ifndef LCD_DRAWING_H
#define LCD_DRAWING_H

#include <uchar.h>
#include "render/render.h"
#include "fonts/font_types.h"

/**
 * @addtogroup render_ll
 * @{
 */

//! Unpacked RGB type
typedef struct {
    float red;   //!< Red   component intensity, 0.0f-1.0f
    float green; //!< Green component intensity, 0.0f-1.0f
    float blue;  //!< Blue  component intensity, 0.0f-1.0f
} rgbf;

//! Text style descriptor
typedef struct {
    int    scale;   //!< How many times to scale up the font
    int    spacing; //!< How many scaled pixel-sized spaces to make between letters
    rgb565 color;   //!< What color to render the text in
    font_t font;    //!< What font to use for the rendering
} text_style;

//! Wrapper around XBM bitmap data
typedef struct {
    int width;             //!< Bitmap width in unscaled pixels
    int height;            //!< Bitmap height in unscaled pixels
    const uint8_t *pixels; //!< Bitmap bits (XBM format)
    int scale;             //!< Bitmap scaling factor
    rgb565 color;          //!< What color to render the image in
} image;

//! Text adjustment mode
typedef enum {
    TEXT_LEFT,   //!< Perform left justification
    TEXT_CENTER, //!< Perform center justification
    TEXT_RIGHT   //!< Perform right justification
} text_adj;

/**
 * @brief Fill the canvas with the given colour.
 * @param frame  Canvas to fill.
 * @param color  Color to fill the canvas with.
 */
extern void draw_clear(lcd_frame *frame, rgb565 color);

/**
 * @brief Calculate text length in pixels.
 * @param line    Line to test.
 * @param style   Style to render the text in.
 */
extern unsigned draw_get_text_len(const char32_t *line, const text_style *style);

/**
 * @brief Draw a character onto the canvas.
 * @param frame   Canvas to draw the character onto.
 * @param x       X coordinate of the top-left character corner.
 * @param y       Y coordinate of the top-left character corner.
 * @param letter  Letter to draw.
 * @param style   Style to render the text in.
 */
extern void draw_text_char(lcd_frame *frame, int x, int y,
                           char32_t letter, const text_style *style);

/**
 * @brief Draw a line of text onto the canvas.
 * @param frame   Canvas to draw the character onto.
 * @param x       X coordinate of the top-left character corner.
 * @param y       Y coordinate of the top-left character corner.
 * @param text    Text to draw. No wrapping is performed.
 * @param style   Style to render the text in.
 */
extern void draw_text_line(lcd_frame *frame, int x, int y,
                           const char32_t *text, const text_style *style);

/**
 * @brief Draw an adjusted line of text onto the canvas.
 * @param frame   Canvas to draw the character onto.
 * @param x       X coordinate of the top-left character corner.
 * @param y       Y coordinate of the top-left character corner.
 * @param w       Horizontal space for adjustments.
 * @param adj     Adjusting mode (left, right or center).
 * @param text    Text to draw. No wrapping is performed.
 * @param style   Style to render the text in.
 */
extern void draw_text_line_adj(lcd_frame *frame, int x, int y, int w, text_adj adj,
                               const char32_t *text, const text_style *style);

/**
 * @brief Draw a XBM image onto the canvas.
 * @param frame   Canvas to draw the character onto.
 * @param x       X coordinate of the top-left character corner.
 * @param y       Y coordinate of the top-left character corner.
 * @param img     Image (meta)data.
 */
extern void draw_image(lcd_frame *frame, int x, int y, const image *img);

/**
 * @brief Draw a filled rectangle onto the canvas.
 * @param frame  Canvas to draw the rectangle onto.
 * @param x0     Top-left pixel X coordinate.
 * @param y0     Top-left pixel Y coordinate.
 * @param w      Width of the rectangle in pixels.
 * @param h      Height of the rectangle in pixels.
 * @param color  Color of the rectangle.
 */
extern void draw_filled_rect(lcd_frame *frame, int x0, int y0, int w, int h,
                             rgb565 color);

/**
 * @brief Draw a hollow rectangle (border only) onto the canvas.
 * @param frame      Canvas to draw the rectangle onto.
 * @param x0         Top-left pixel X coordinate.
 * @param y0         Top-left pixel Y coordinate.
 * @param w          Width of the rectangle in pixels.
 * @param h          Height of the rectangle in pixels.
 * @param thickness  How thick the border should be.
 * @param color      Color of the rectangle.
 */
extern void draw_hollow_rect(lcd_frame *frame, int x0, int y0, int w, int h,
                             int thickness, rgb565 color);

/**
 * @brief Draw a horizontal line onto the canvas.
 * @param frame   Canvas to draw the character onto.
 * @param x0      Left pixel X coordinate.
 * @param y0      Left pixel Y coordinate.
 * @param length  Length of the line (left-right).
 * @param color   Color of the line.
 */
extern void draw_horizontal_line(lcd_frame *frame, int x0, int y0, int length, rgb565 color);

/**
 * @brief Draw a vertical line onto the canvas.
 * @param frame   Canvas to draw the character onto.
 * @param x0      Top pixel X coordinate.
 * @param y0      Top pixel Y coordinate.
 * @param length  Length of the line (top-left).
 * @param color   Color of the line.
 */
extern void draw_vertical_line(lcd_frame *frame, int x0, int y0, int length, rgb565 color);


/**
 * @brief Set a pixel on the canvas.
 * @param canvas Canvas where to set the pixel.
 * @param x      X coordinate of the pixel to set (horizontal distance from top-left corner).
 * @param y      Y coordinate of the pixel to set (vertical distance from top-left corner).
 * @param pixel  Pixel value to set.
 */
static inline void pixel_set(lcd_frame *canvas, int x, int y, rgb565 pixel);

/**
 * @brief Get a pixel from the canvas.
 * @param canvas Canvas from which to get the pixel.
 * @param x      X coordinate of the pixel to get (horizontal distance from top-left corner).
 * @param y      Y coordinate of the pixel to get (vertical distance from top-left corner).
 */
static inline rgb565 pixel_get(lcd_frame *canvas, int x, int y);

/**
 * @brief Pack a pixel to the rgb565 format.
 * @param wide RGB components as floats
 * @returns RGB565 pixel
 */
static inline rgb565 pixel_pack(rgbf wide);

/**
 * @brief Unpack a pixel from the rgb565 fromat.
 * @param narrow RGB565 packed pixel
 * @returns RGB components as floats
 */
static inline rgbf pixel_unpack(rgb565 narrow);

// inline functions for better performance
#include "drawing_pixel.h"

/**@}*/
#endif // LCD_DRAWING_H
