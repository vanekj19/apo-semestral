/**
 * @file motors.c
 * @brief Management of the regulation thread
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup motor_thread
 */

#include <pthread.h>
#include <errno.h>

#include "core/logging.h"
#include "core/common.h"
#include "motors/motors.h"
#include "motors/regulator.h"
#include "build_config.h"

//! Control thread mainloop. motor_ctx expected as an argument.
static void *motor_thread(void *ctx);

//! Start the regulation thread.
static bool init_thread(motor_ctx *state);


////////////////////////////////////////////////////////////////
// Thread/regulator startup
//

bool motor_thread_start(motor_ctx *state)
{
    log_debug("initializing regulation thread");
    state->reg_started = false;
    state->do_run      = true;

    if (!motor_init_reg(state)) { // in regulator.c
        return false;
    }

    return init_thread(state);
}

//! Helper macro for handling pthread function failures (exceptions ftw)
#define PTHREAD_CHECK(error, msg) do { \
        if ((error) != 0) {            \
            errno = (error);           \
            log_sys_error((msg));      \
            ok = false;                \
            goto cleanup;              \
        }                              \
    } while(0)

bool init_thread(motor_ctx *state)
{
    pthread_attr_t attrs;
    bool ok = true;
    bool attrs_allocated = false;
    int error;

    struct sched_param schedinfo = { .sched_priority = MOTOR_REQ_PRIORITY };

    error = pthread_attr_init(&attrs);
    PTHREAD_CHECK(error, "cannot initialize pthread attributes");
    attrs_allocated = true;

    error = pthread_attr_setinheritsched(&attrs, PTHREAD_EXPLICIT_SCHED);
    PTHREAD_CHECK(error, "cannot override scheduler");

    error = pthread_attr_setschedpolicy(&attrs, SCHED_RR);
    PTHREAD_CHECK(error, "cannot enable realtime scheduling");

    error = pthread_attr_setschedparam(&attrs, &schedinfo);
    PTHREAD_CHECK(error, "cannot set thread priotity");

    error = pthread_create(&state->reg_thread, &attrs, motor_thread, state);
    PTHREAD_CHECK(error, "cannot start motor thread");
    state->reg_started = true;

cleanup:
    if (attrs_allocated) {
        pthread_attr_destroy(&attrs);
    }
    return ok;
}
#undef PTHREAD_CHECK


////////////////////////////////////////////////////////////////
// Thread/regulator shutdown
//

bool motor_thread_join(motor_ctx *state)
{
    bool ok = true;

    log_debug("joining regulation thread");

    state->do_run = false;

    if (state->reg_started) {
        int status = pthread_join(state->reg_thread, NULL);
        if (status != 0) {
            errno = status;
            log_sys_warn("couldn't join motor thread");
            ok = false;
        }

        state->reg_started = false;
    }

    motor_deinit_reg(state); // in regulator.c
    return ok;
}


////////////////////////////////////////////////////////////////
// Thread mainloop
//

void *motor_thread(void *ctx)
{
    pthread_setname_np(pthread_self(), "regulator");

    motor_ctx *data = (motor_ctx *) ctx;

    struct timespec last_time = now_monotonic();

    log_info("starting regulation loop");
    while (data->do_run) {
        motor_regulate(data); // in regulator.c
        loop_delay(&last_time, MOTOR_REG_FREQ);
    }
    log_info("leaving regulation loop");

    return NULL;
}
