/**
 * @file regulator.h
 * @brief Internal regulation submodule declarations
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup motor_reg
 */

#ifndef MOTORS_REG_H
#define MOTORS_REG_H

#include "motors/motors.h"

/**
 * @addtogroup motor_reg
 * @{
 */

/**
 * @brief Initialize the regulator submodule.
 * @param state Motor module state.
 * @returns Whether the initialization succeeded or not.
 */
extern bool motor_init_reg(motor_ctx *state);

/**
 * @brief Deinitialize the regulator submodule (including the motor control hardware).
 * @param state Motor module state.
 */
extern void motor_deinit_reg(motor_ctx *state);

/**
 * @brief Run one iteration of the regulation loop.
 * @param state Motor module state.
 */
extern void motor_regulate(motor_ctx *state);

/**@}*/
#endif // MOTORS_REG_H
