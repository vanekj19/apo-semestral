/**
 * @file motors.h
 * @brief Public API of the motor controller module
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup motor
 */

#ifndef MOTORS_INTERFACE_H
#define MOTORS_INTERFACE_H

#include <pthread.h>
#include "core/common.h"
#include "core/mmio_regs.h"

/**
 * @addtogroup motor
 * @{
 */

//! Motor controller module variables
typedef struct {
    motor_if    *hw;         //!< Motor controller

    atomic_int  target_pos;  //!< Regulation target position.

    pthread_t   reg_thread;  //!< Regulation thread.
    bool        reg_started; //!< Whether the regulation thread was started.
    atomic_bool do_run;      //!< Whether the regulation thread should continue to run.
} motor_ctx;

/**
 * @brief Initialize the motor controller and start the regulation thread.
 * @param state Regulation thread context.
 * @returns     Whether the operation succeeded.
 */
extern bool motor_thread_start(motor_ctx *state);

/**
 * @brief Stop the motor regulation thread and deinitialize the motor controller.
 * @param state Regulation thread context.
 * @returns     Whether the operation succeeded or not.
 */
extern bool motor_thread_join(motor_ctx *state);

/**
 * @brief Set the regulated motor position.
 * @param state  Regulation thread context.
 * @param locked Whether the door should be locked or not.
 */
extern void motor_set_position(motor_ctx *state, bool locked);

/**@}*/
#endif // MOTORS_INTERFACE_H
