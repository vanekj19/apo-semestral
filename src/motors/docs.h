/**
 * @defgroup motor Motor control module
 * @brief Motor controller infrastructure
 *
 * This module provides a high-level API for the actual locking process.
 * Locking is represented by the position of a DC motor connected
 * to MZ_APO. There are two defined positions - locked and unlocked.
 * At start, it is assumed that the motor encoder is at position 0.
 * Determining what this means with regards to the lock status
 * is left to the application (currently it starts with locked lock).
 */

/**
 * @defgroup motor_thread Threading
 * @brief Management of a separate realtime regulation thread
 * @ingroup motor
 */

/**
 * @defgroup motor_reg Motor P regulator
 * @brief Position regulator of the effector motor
 * @ingroup motor
 */
