/**
 * @file regulator.c
 * @brief Simple proportional regulator for the MZ_APO motor
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup motor_reg
 */

#include "motors/regulator.h"
#include "core/mmio_map.h"
#include "core/logging.h"
#include "build_config.h"
#include "config/config.h"

//! Set PWM duty cycle, power is in -1.0f ~ +1.0f range
static void     set_pwm(motor_if *hw, float power);

//! Convert power (-1.0f ~ +1.0f) to actual PWM timer period
static uint32_t get_pwm_on_time(float power);


////////////////////////////////////////////////////////////////
// Initialization/deinitialization
//

bool motor_init_reg(motor_ctx *state)
{
    motor_if *hw = mmio_map(MOTOR_HWADDR, MOTOR_HWSIZE, false);
    if (hw == NULL) {
        return false;
    }

    mmio_write32(hw, MOTOR_CONTROL, MOTOR_CTRL_IRC_RESET);
    mmio_write32(hw, MOTOR_DUTY,    0);
    mmio_write32(hw, MOTOR_PERIOD,  MOTOR_TIMER_CLOCK / MOTOR_PWM_FREQ);
    mmio_write32(hw, MOTOR_CONTROL, MOTOR_CTRL_PWM_ENABLE);

    state->target_pos  = CONFIG.motor_pos_locked;
    state->hw          = hw;
    return true;
}

void motor_deinit_reg(motor_ctx *state)
{
    if (state->hw != NULL) {
        mmio_write32(state->hw, MOTOR_DUTY,    0);
        mmio_write32(state->hw, MOTOR_CONTROL, 0);
        mmio_unmap(state->hw, MOTOR_HWSIZE);
        state->hw = NULL;
    }
}


////////////////////////////////////////////////////////////////
// Regulation
//

void motor_regulate(motor_ctx *state)
{
    const float power_max = CONFIG.motor_reg_pwr_max;
    const float power_0   = CONFIG.motor_reg_pwr_min;
    const float kp        = CONFIG.motor_reg_kp;

    int target  = state->target_pos;
    int current = mmio_read32(state->hw, MOTOR_IRC);
    int error = target - current;

    float action = error * kp;
    if (action > 0.0f)  action += power_0;
    if (action < 0.0f)  action -= power_0;

    action = clipf(action, -power_max, +power_max);

    set_pwm(state->hw, action);
}

void motor_set_position(motor_ctx *state, bool locked)
{
    log_debug("setting motor to %s position", locked ? "LOCKED" : "UNLOCKED");
    int new_target = locked ? CONFIG.motor_pos_locked : CONFIG.motor_pos_unlocked;

    state->target_pos = new_target;
}


////////////////////////////////////////////////////////////////
// Motor control helpers
//

uint32_t get_pwm_on_time(float power)
{
    float clipped = clipf(power, 0.0f, 1.0f);

    uint32_t result = (uint32_t) (clipped * MOTOR_TIMER_CLOCK / MOTOR_PWM_FREQ);

    return result & MOTOR_DUTY_MASK;
}

void set_pwm(motor_if *motor, float power)
{
    uint32_t dir;

    if (power >= 0.0f) {
        dir = MOTOR_DUTY_FWD;
    } else {
        power = -power;
        dir = MOTOR_DUTY_BWD;
    }

    mmio_write32(motor, MOTOR_DUTY, get_pwm_on_time(power) | dir);
}
