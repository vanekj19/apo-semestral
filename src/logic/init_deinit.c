/**
 * @file init_deinit.c
 * @brief program initialization/deinitialization logic
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logic_init
 */

#include <time.h>

#include "logic/logic.h"
#include "core/logging.h"
#include "build_config.h"

bool program_init(program_state *data)
{
    struct timespec now = now_monotonic();

    data->state = LOCK_LOCKED;
    data->state_since   = now;
    data->animation_ref = now;
    data->do_run        = true;

    if ((data->hw_lcd = lcd_init()) == NULL) {
        log_warn("couldn't initialize lcd driver");
        return false;
    }

    if ((data->hw_led = led_init()) == NULL) {
        log_warn("couldn't initialize led driver");
        return false;
    }

    if (!encoder_init(&data->enc)) {
        log_warn("couldn't initialize encoder driver");
        return false;
    }

    if (!motor_thread_start(&data->motor_ctrl)) {
        log_warn("motor regulator startup failed");
        return false;
    }

    return true;
}

void program_deinit(program_state *data)
{
    data->do_run = false;

    // lock the lock before exiting
    if (data->motor_ctrl.reg_started) {
        motor_set_position(&data->motor_ctrl, true);
        delay_ms(1000);
    }
    motor_thread_join(&data->motor_ctrl);

    led_deinit(data->hw_led);
    lcd_deinit(data->hw_lcd);
    encoder_deinit(&data->enc);
    data->hw_led = NULL;
    data->hw_lcd = NULL;
}
