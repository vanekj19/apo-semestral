/**
 * @file logic.h
 * @brief Public API / declarations of the main business logic module
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logic
 */

#ifndef GUI_INTERFACE_H
#define GUI_INTERFACE_H

#include "core/common.h"
#include "lcd/lcd.h"
#include "motors/motors.h"
#include "leds/leds.h"
#include "encoders/encoders.h"
#include "core/defs.h"

/**
 * @addtogroup logic
 * @{
 */

//! Overall program state.
typedef struct {
    ui_state        state;          //!< Current UI mode (locked, unlocked, unlock failed)
    struct timespec state_since;    //!< When was the current UI mode set
    struct timespec animation_ref;  //!< Time reference for animation counter
    encoders        enc;            //!< Encoder tracking state
    motor_ctx       motor_ctrl;     //!< Motor controller

    lcd_if          *hw_lcd;        //!< LCD controller
    led_if          *hw_led;        //!< LED controller

    atomic_bool     do_run;         //!< Run flag (is set to false by pressing Ctrl+C)

    lcd_frame       current_frame;  //!< LCD rendering canvas
} program_state;


/**
 * @brief Initialize the program.
 * @param data Program data structures.
 * @returns Whether the initialization succeeded or failed.
 */
extern bool program_init(program_state *data);

/**
 * @brief Enter the program mainloop.
 * @param data Program data structures.
 */
extern void program_enter(program_state *data);

/**
 * @brief Deinitialize the program.
 * @param data Program data structures.
 */
extern void program_deinit(program_state *data);

/**@}*/
#endif // GUI_INTERFACE_H
