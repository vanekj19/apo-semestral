/**
 * @file main_flow.c
 * @brief lock state transitioning logic
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logic
 */

#include <unistd.h>

#include "logic/logic.h"
#include "logic/logic_internal.h"
#include "core/logging.h"
#include "core/common.h"
#include "config/config.h"

static void set_state(program_state *prg, ui_state state);
static void process_locked(program_state *prg);
static void process_unlocked(program_state *prg);
static void process_unlock_failed(program_state *prg);


////////////////////////////////////////////////////////////////
// Main loop
//

void program_enter(program_state *data)
{
    struct timespec last_iter = now_monotonic();

    while (data->do_run) {
        encoder_update(&data->enc);

        switch (data->state) {
        case LOCK_LOCKED:
            process_locked(data);
            break;
        case LOCK_UNLOCKED:
            process_unlocked(data);
            break;
        case LOCK_UNLOCK_FAILED:
            process_unlock_failed(data);
            break;
        default:
            log_unreachable("invalid lock state");
            break;
        }

        generic_ui_update(data);
        loop_delay(&last_iter, GUI_FPS);
    }
}


////////////////////////////////////////////////////////////////
// Locked lock processing
//

void process_locked(program_state *prg)
{
    update_led_idle(prg);
    if (!prg->enc.current.lock_pressed) {
        return; // no button is pressed
    }

    if (time_compare(&prg->enc.current.lock_pressed_since, &prg->state_since) <= 0) {
        return; // button was pressed before this state was entered
    }

    log_info("checking code");
    if (prg->enc.current.code == CONFIG.correct_code) {
        log_info("code is OK");
        motor_set_position(&prg->motor_ctrl, false);
        set_state(prg, LOCK_UNLOCKED);
    } else {
        log_warn("code is incorrect, showing warning");
        set_state(prg, LOCK_UNLOCK_FAILED);
    }
}


////////////////////////////////////////////////////////////////
// Unlock failed warning processing
//

void process_unlock_failed(program_state *prg)
{
    led_line_draw_raw(prg->hw_led, LED_UNLOCK_FAIL_PATTERN);
    if (millis_from(&prg->state_since) > UNLOCK_WARN) {
        log_debug("hiding unlock warning");
        set_state(prg, LOCK_LOCKED);
    }
}


////////////////////////////////////////////////////////////////
// Unlocked lock processing
//

void process_unlocked(program_state *prg)
{
    bool do_lock = false;

    bool status = update_led_timeout(prg);
    if (status == TIMEOUT_ELAPSED) {
        log_info("lock timer timed out");
        do_lock = true;
    }

    if (time_compare(&prg->enc.current.lock_pressed_since, &prg->state_since) > 0) {
        log_info("user requested locking");
        do_lock = true;
    }

    if (do_lock) {
        log_info("locking the lock");
        motor_set_position(&prg->motor_ctrl, true);
        encoder_reset(&prg->enc);
        set_state(prg, LOCK_LOCKED);
    }
}


////////////////////////////////////////////////////////////////
// Helper for transitioning between states
//

void set_state(program_state *prg, ui_state state)
{
    if (prg->state != state) {
        prg->state = state;
        prg->state_since = now_monotonic();
        reset_led_animation(prg);
    }
}
