/**
 * @file logic_internal.h
 * @brief UI update helper declarations
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logic_ui
 */

#ifndef GUI_INTERFACE_INTERNAL_H
#define GUI_INTERFACE_INTERNAL_H

#include "logic/logic.h"

/**
 * @addtogroup logic_ui
 * @{
 */

/**
 * @name helper macros for update_led_timeout()
 * @{
 */
//! Timeout has elapsed
#define TIMEOUT_ELAPSED false
//! Timeout hasn't elapsed yet
#define TIMEOUT_PENDING true
/**@}*/

/**
 * @brief Reset LED animation counter.
 * @param prg Pointer to program state.
 */
extern void reset_led_animation(program_state *prg);

/**
 * @brief Update the yellow LED line with locking timeout progress bar.
 * @param prg Pointer to program state.
 * @returns TIMEOUT_* depending on whether the timeout has expired or not.
 */
extern bool update_led_timeout(program_state *prg);

/**
 * @brief Update the yellow LED line with an idle animation.
 * @param prg Pointer to program state.
 */
extern void update_led_idle(program_state *prg);

/**
 * @brief Update the display and RGB LEDs with current state.
 * @param prg Pointer to program state.
 */
extern void generic_ui_update(program_state *prg);

/**@}*/
#endif // GUI_INTERFACE_INTERNAL_H
