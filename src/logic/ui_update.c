/**
 * @file ui_update.c
 * @brief code for updating the UI using other modules
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup logic_ui
 */

#include "logic/logic.h"
#include "logic/logic_internal.h"
#include "render/render.h"
#include "build_config.h"
#include "config/config.h"


////////////////////////////////////////////////////////////////
// LED line utilities
//

bool update_led_timeout(program_state *prg)
{
    int elapsed = millis_from(&prg->state_since);

    int total_time     = CONFIG.unlocked_timeout;
    int remaining_msec = clip(total_time - elapsed, 0, total_time);
    int remaining_leds = remaining_msec * YLEDS / total_time;

    led_line_draw_timeout(prg->hw_led, remaining_leds);

    return remaining_msec == 0 ? TIMEOUT_ELAPSED : TIMEOUT_PENDING;
}


void update_led_idle(program_state *prg)
{
    long seed = millis_from(&prg->animation_ref);
    int anim_step = (seed * LED_ANIMATION_STEPS / ANIMATION_PERIOD) % LED_ANIMATION_STEPS;

    led_line_draw_animation(prg->hw_led, anim_step);
}

void reset_led_animation(program_state *prg)
{
    prg->animation_ref = now_monotonic();
}


////////////////////////////////////////////////////////////////
// UI update utilities
//

void generic_ui_update(program_state *prg)
{
    time_t now = time(NULL);
    struct tm now_exploded;
    localtime_r(&now, &now_exploded);

    screen_state params;
    params.code        = prg->enc.current.code;
    params.lock_status = prg->state;
    params.clock_time  = now_exploded;

    render_screen(&prg->current_frame, &params);
    lcd_write(prg->hw_lcd, &prg->current_frame);
    led_rgb_set(prg->hw_led, prg->state);
}
