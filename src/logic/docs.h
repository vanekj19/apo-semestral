/**
 * @defgroup logic Application logic
 * @brief Application flows and logic
 *
 * This module manages the lock state and glues most of all other modules
 * together. It works by implementing a mainloop and updating/checking
 * other modules along the way.
 */

/**
 * @defgroup logic_init Program initialization
 * @brief Logic for initialization of all other modules
 * @ingroup logic
 */

/**
 * @defgroup logic_ui UI update logic
 * @brief Helper functions for updating UI (LCD drawing, LED animation, timeouts)
 * @ingroup logic
 */
