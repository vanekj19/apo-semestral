/**
 * @defgroup config Configuration module
 * @brief Configuration management (loading, saving, validation)
 *
 * This module provides a shared configuration storage.
 * It also provides functions to import and export configuration
 * to a configuration file.
 *
 * The configuration file format is a based on key=value pairs.
 * Comments are also supported; everything after any '#' character is ignored.
 * Supported value types are integers, floats, strings and bools.
 *  - Strings can either unquoted or quoted; the quotes are stripped.
 *  - Integers and floats must be unquoted and follow a C-like syntax.
 *    Optional comment-like suffix at the end is allowed and ignored.
 *  - Bools must be unquoted and written in lowercase.
 *    'yes', 'on', 'true' and '1' (without quotes) are accepted as true.
 *    'no', 'off', 'false' and '0' (without quotes) are accepted as false.
 */

/**
 * @defgroup config_import Configuration loader
 * @brief Functions for loading a configuration file
 * @ingroup config
 */

/**
 * @defgroup config_import_helpers Parsing & serialization helpers
 * @brief Functions for string handling and expression parsing
 * @ingroup config_import
 */

/**
 * @defgroup config_export Configuration saver/exporter.
 * @brief Functions for saving a configuration file
 * @ingroup config
 */

/**
 * @defgroup config_valid Configuration validator
 * @brief Functions to check if a configuration is acceptable or not
 * @ingroup config
 */
