/**
 * @file config_parser.c
 * @brief Top-level parser for config files
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config_import
 */

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "config/config.h"
#include "config/config_helpers.h"
#include "core/logging.h"

//! Load config from a pre-opened file
static bool do_load(FILE *fp, app_config *cfg);


////////////////////////////////////////////////////////////////
// Top-level flow & file loading
//

bool config_load(const char *file, app_config *cfg)
{
    log_info("loading configuration from %s", file);
    FILE *fp = fopen(file, "r");
    if (fp == NULL) {
        log_sys_error("cannot open config file %s for reading", file);
        return false;
    }

    bool ok = do_load(fp, cfg);
    fclose(fp);

    if (ok) {
        const char *problem = NULL;
        ok = config_validate(cfg, &problem);
        if (!ok) {
            log_error("config validation failed: %s", problem);
        }
    }

    return ok;
}

bool do_load(FILE *fp, app_config *cfg)
{
    char *line = NULL;
    size_t line_cap = 0;
    ssize_t chars;
    bool ok = true;
    int lineno = 1;

    while ((chars = getline(&line, &line_cap, fp)) >= 0) {
        if (!config_parse_line(line, lineno, cfg)) {
            ok = false;
            break;
        }
        lineno++;
    }

    ok = ok && feof(fp);

    free(line);
    return ok;
}
