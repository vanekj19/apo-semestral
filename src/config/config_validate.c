/**
 * @file config_validate.c
 * @brief Configuration file validation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config_valid
 */

#include "config/config.h"
#include <stddef.h>

#define CHECK(cond, msg) do {      \
        if (!(cond)) {             \
            if (reason != NULL) {  \
                *reason = (msg);   \
            }                      \
            return false;          \
        }                          \
    } while(0)

bool config_validate(const app_config *cfg, const char **reason)
{
    CHECK(cfg->correct_code            >= 0,   "code must be non-negative");
    CHECK(cfg->correct_code            <  1000, "code must have 3 digits max");
    CHECK(cfg->unlocked_timeout        >  1000, "lock must be unlocked for at least one second");
    CHECK(cfg->encoder_ticks_per_digit != 0,    "at least one encoder tick per digit is needed");
    CHECK(cfg->motor_reg_pwr_min       >= 0.0f, "deadband power must be nonnegative");
    CHECK(cfg->motor_reg_pwr_min       <= 1.0f, "deadband power must be smaller than one");
    CHECK(cfg->motor_reg_pwr_max       >= 0.0f, "max power must be nonnegative");
    CHECK(cfg->motor_reg_pwr_max       <= 1.0f, "max power must be smaller than one");
    CHECK(cfg->motor_reg_pwr_min < cfg->motor_reg_pwr_max, "max power must be smaller min power");
    return true;
}

#undef CHECK
