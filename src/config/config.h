/**
 * @file config.h
 * @brief Public API of the configuration module
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config
 */

#ifndef CONFIG_CONFIG_H
#define CONFIG_CONFIG_H

#include <stdint.h>
#include <stdbool.h>
#include <uchar.h>
#include "build_config.h"


/**
 * @addtogroup config
 * @{
 */
/**
 * @brief Structure for storing volatile application configuration.
 */
typedef struct {
    //! Code for unlocking the lock
    int correct_code;

    //! How many milliseconds does the user have until the lock is locked again
    int unlocked_timeout;
    //! How many encoder ticks are necessary for a lock digit to change
    int encoder_ticks_per_digit;

    //! Whether to use old display (HX8357-C) or new display (ILI9481)
    bool new_display;
    //! Whether to emulate encoders using stdin or not
    bool emulate_encoders;

    //! Proportional lock motor regulator constant
    float motor_reg_kp;
    //! Deadband power always applied to the motor (0.0f-1.0f)
    float motor_reg_pwr_min;
    //! Maximum power applied to the lock motor (0.0f-1.0f)
    float motor_reg_pwr_max;

    //! Encoder position for a locked lock.
    int motor_pos_locked;
    //! Encoder position for an unlocked lock.
    int motor_pos_unlocked;

    //! UI string: lock is unlocked
    char32_t unlocked_text[MAX_CONFIG_STR_LEN];
    //! UI string: lock is locked
    char32_t locked_text[MAX_CONFIG_STR_LEN];
    //! UI string: incorrect code entered
    char32_t incorrect_code_text[MAX_CONFIG_STR_LEN];
    //! UI string: lock location/name
    char32_t lock_name[MAX_CONFIG_STR_LEN];
} app_config;


//! Current application configuration.
extern app_config CONFIG;

//! Default application configuration.
extern const app_config DEFAULT_CONFIG;


/**
 * @brief Load a configuration from a configuration file.
 * @param file  Path to the file.
 * @param cfg   Configuration struct to fill in (warning: will be clobbered
 *              even if the loading fails).
 * @returns Whether the loading succeeded or not.
 */
extern bool config_load(const char *file, app_config *cfg);

/**
 * @brief Store a configuration to a configuration file.
 * @param file  Path to the file to fill in.
 * @param cfg   Configuration struct to use.
 * @returns Whether the saving succeeded or not.
 */
extern bool config_save(const char *file, const app_config *cfg);

/**
 * @brief Check if the data contained in the configuration struct is sane.
 * @param cfg     Configuration struct to check.
 * @param reason  Where to store why the config is invalid; can be NULL.
 * @returns Whether all data passes all checks or not.
 */
extern bool config_validate(const app_config *cfg, const char **reason);

/**@}*/
#endif // CONFIG_CONFIG_H
