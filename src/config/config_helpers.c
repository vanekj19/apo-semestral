/**
 * @file config_helpers.c
 * @brief String handling helper functions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config_import_helpers
 */

#include "config/config_helpers.h"
#include "core/logging.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <errno.h>
#include <stdlib.h>


////////////////////////////////////////////////////////////////
// Enhanced "string.h"-like functions
//

size_t strlen_without_whitespace(const char *str)
{
    const char *ptr = str;
    size_t count = 0;
    while (*ptr != '\0') {
        if (!isspace(*ptr)) {
            count++;
        }
        ptr++;
    }
    return count;
}

char *skip_spaces(const char *start, const char *end)
{
    const char *ptr = start;
    while (isspace(*ptr) && *ptr != '\0' && ptr != end) {
        ptr++;
    }
    return (char*) ptr;
}

char *skip_spaces_rev(const char *begin, const char *end)
{
    const char *ptr = end;
    while (ptr > begin) {
        if (!isspace(ptr[-1])) {
            break;
        }
        ptr--;
    }

    return (char*) ptr;
}

size_t double_trim(char **p_start, char **p_end)
{
    *p_start = skip_spaces(*p_start, *p_end);
    *p_end = skip_spaces_rev(*p_start, *p_end);
    return *p_end - *p_start;
}

bool keys_same(const char *a, const char *b)
{
    return strcmp(a, b) == 0;
}


////////////////////////////////////////////////////////////////
// Value parsing wrappers
//

bool parse_int(const char *str, int *p_value)
{
    char *endptr = NULL;
    errno = 0;
    long value = strtol(str, &endptr, 10);

    if (str == endptr) {
        return false; // zero-length match = parse failure
    }

    if (errno != 0 || value <= INT_MIN || value >= INT_MAX) {
        return false; // overflow
    }

    *p_value = value;
    return true;
}

bool parse_float(const char *str, float *p_value)
{
    char *endptr = NULL;
    errno = 0;
    float value = strtof(str, &endptr);

    if (str == endptr) {
        return false; // zero-length match = parse failure
    }

    if (errno != 0) {
        return false; // overflow
    }

    *p_value = value;
    return true;
}

bool parse_string(char *str, char32_t *buffer)
{
    char *end = str + strlen(str);
    int len = double_trim(&str, &end);

    bool quoted = str[0] == '"';
    if (quoted) {
        if (str[len - 1] == '"') {
            str[len - 1] = '\0';
            str += 1;
            len -= 2;
        } else {
            return false;
        }
    }

    if (len + 1 > MAX_CONFIG_STR_LEN) {
        log_error("string is too long");
        return false;
    }

    return u8_to_u32(str, buffer, MAX_CONFIG_STR_LEN);
}

bool parse_bool(char *str, bool *p_value)
{
    bool ok = true;
    if (strcmp(str, "on") == 0 ||
        strcmp(str, "yes") == 0 ||
        strcmp(str, "true") == 0 ||
        strcmp(str, "1") == 0) {
        *p_value = true;

    } else if (strcmp(str, "off") == 0 ||
               strcmp(str, "no") == 0 ||
               strcmp(str, "false") == 0 ||
               strcmp(str, "0") == 0) {
        *p_value = false;

    } else {
        log_error("cannot parse boolean: %s", str);
        ok = false;
    }
    return ok;
}
