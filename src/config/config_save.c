/**
 * @file config_save.c
 * @brief Configuration file exporting
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config_export
 */

#include "core/logging.h"
#include "config/config.h"
#include "config/config_helpers.h"
#include <stdio.h>
#include <stdlib.h>

//! save all values to an opened file
static bool do_save(FILE *fp, const app_config *cfg);

//! save one float value
static bool do_save_float(FILE *fp, const char *name, float value);

//! save one integer value
static bool do_save_int(FILE *fp, const char *name, int value);

//! save one integer value
static bool do_save_bool(FILE *fp, const char *name, bool value);

//! save one string value
static bool do_save_str(FILE *fp, const char *name, const char32_t *value);


////////////////////////////////////////////////////////////////
// Top-level flow & file handling
//

bool config_save(const char *file, const app_config *cfg)
{
    log_info("saving configuration to %s", file);
    const char *why = NULL;
    if (!config_validate(cfg, &why)) {
        log_error("will not save invalid configuration: %s", why);
        return false;
    }

    FILE *fp = fopen(file, "w");
    if (fp == NULL) {
        log_sys_error("cannot open config file %s for writing", file);
        return false;
    }

    bool ok = do_save(fp, cfg);

    fclose(fp);
    return ok;
}

#define CHECKED(x) do { if (!(x)) return false; } while (0)

bool do_save(FILE *fp, const app_config *cfg)
{
    CHECKED(do_save_int  (fp, "correct code",            cfg->correct_code));
    CHECKED(do_save_int  (fp, "unlocked timeout",        cfg->unlocked_timeout));
    CHECKED(do_save_int  (fp, "encoder ticks per digit", cfg->encoder_ticks_per_digit));
    CHECKED(do_save_int  (fp, "motor locked pos",        cfg->motor_pos_locked));
    CHECKED(do_save_int  (fp, "motor unlocked pos",      cfg->motor_pos_unlocked));
    CHECKED(do_save_bool (fp, "drive new display",       cfg->new_display));
    CHECKED(do_save_bool (fp, "emulate encoders",        cfg->emulate_encoders));
    CHECKED(do_save_float(fp, "regulator kp",            cfg->motor_reg_kp));
    CHECKED(do_save_float(fp, "regulator pwr min",       cfg->motor_reg_pwr_min));
    CHECKED(do_save_float(fp, "regulator pwr max",       cfg->motor_reg_pwr_max));
    CHECKED(do_save_str  (fp, "text for locked",         cfg->locked_text));
    CHECKED(do_save_str  (fp, "text for unlocked",       cfg->unlocked_text));
    CHECKED(do_save_str  (fp, "text for incorrect code", cfg->incorrect_code_text));
    CHECKED(do_save_str  (fp, "text for lock name",      cfg->lock_name));
    return true;
}

#undef CHECKED


////////////////////////////////////////////////////////////////
// Key-value pair handling
//

bool do_save_float(FILE *fp, const char *name, float value)
{
    if (fprintf(fp, "%s = %f", name, value) < 0) {
        log_sys_error("cannot store value %s = %f", name, value);
        return false;
    }
    return true;
}

bool do_save_int(FILE *fp, const char *name, int value)
{
    if (fprintf(fp, "%s = %d", name, value) < 0) {
        log_sys_error("cannot store value %s = %d", name, value);
        return false;
    }
    return true;
}

bool do_save_str(FILE *fp, const char *name, const char32_t *value)
{
    char utf8[MAX_CONFIG_STR_LEN];

    if (!u32_to_u8(value, utf8, MAX_CONFIG_STR_LEN)) {
        log_sys_error("cannot convert value of key '%s' to UTF-8", name);
        return false;
    }

    if (fprintf(fp, "%s = \"%s\"", name, utf8) < 0) {
        log_sys_error("cannot store value %s = \"%s\"", name, utf8);
        return false;
    }
    return true;
}

bool do_save_bool(FILE *fp, const char *name, bool value)
{
    const char *strval = value ? "yes" : "no";
    if (fprintf(fp, "%s = %s", name, strval) < 0) {
        log_sys_error("cannot store value %s = %s", name, strval);
        return false;
    }
    return true;
}
