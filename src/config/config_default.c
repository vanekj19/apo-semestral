/**
 * @file config_default.c
 * @brief Definition of default configuration values
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config
 */

#include "config/config.h"

/**
 * @addtogroup config
 * @{
 */
const app_config DEFAULT_CONFIG = {
    .correct_code = 123,

    .unlocked_timeout = 32000,
    .encoder_ticks_per_digit = 5,

    .new_display = false,
    .emulate_encoders = false,

    .motor_reg_kp = 0.001f,
    .motor_reg_pwr_min = 0.03f,
    .motor_reg_pwr_max = 0.13f,

    .motor_pos_locked   =   0,
    .motor_pos_unlocked = 256,

#ifdef DEFAULT_LANG_CZ
    .unlocked_text       = U"Odemčeno",
    .locked_text         = U"Uzamčeno",
    .incorrect_code_text = U"Špatný kód",
    .lock_name           = U"MZ_APO zámek"
#else
    .unlocked_text       = U"Unlocked",
    .locked_text         = U"Locked",
    .incorrect_code_text = U"Incorr. PIN",
    .lock_name           = U"MZ_APO Lock"
#endif
};

app_config CONFIG;

/**@}*/
