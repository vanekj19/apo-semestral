/**
 * @file config_helpers.h
 * @brief String handling helper functions
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config_import_helpers
 */

#ifndef CONFIG_CONFIG_INTERNAL_H
#define CONFIG_CONFIG_INTERNAL_H

#include <stddef.h>
#include <stdbool.h>
#include "core/utf8.h"
#include "config/config.h"

/**
 * @addtogroup config_import_helpers
 * @{
 */

/**
 * @brief Parse one line from the config file.
 * @param line  Line text.
 * @param num   Current line number.
 * @param cfg   Configuration to update with the parsed values.
 * @returns     Whether the parsing succeeded dor not.
 */
extern bool config_parse_line(char *line, int num, app_config *cfg);

/**
 * @brief Get string length without counting whitespace characters.
 * @param str String where to count the characters.
 * @returns Number of nonspace characters in the string.
 */
extern size_t strlen_without_whitespace(const char *str);

/**
 * @brief Skip whitespace characters at the start of a string.
 * @param start  String to scan.
 * @param end    End of the string.
 * @returns      Pointer to first non-whitespace character or null terminator.
 */
extern char *skip_spaces(const char *start, const char *end);

/**
 * @brief Skip whitespace characters at the end of a string.
 * @param begin  Beginning of the string.
 * @param end    End of the string (trimming starting position).
 * @returns      Pointer to last non-whitespace character.
 */
extern char *skip_spaces_rev(const char *begin, const char *end);

/**
 * @brief Trim the string (eliminate spaces at the start and end).
 * @param p_start Writable pointer to the start of the string.
 * @param p_end   Writable pointer to the past-the-end character.
 * @returns Length of the trimmed string.
 */
extern size_t double_trim(char **p_start, char **p_end);

/**
 * @brief Check if two config keys are the same.
 * @param a First string to check.
 * @param b Second string to check.
 * @returns True if they're the same, false otherwise.
 */
extern bool keys_same(const char *a, const char *b);

/**
 * @brief Parse a signed integer.
 * @param str     String to parse.
 * @param p_value Resulting value (not modified if parsing failed).
 * @returns Whether the parsing succeeded or not.
 */
extern bool parse_int(const char *str, int *p_value);

/**
 * @brief Parse a floating point number.
 * @param str     String to parse.
 * @param p_value Resulting value (not modified if parsing failed).
 * @returns Whether the parsing succeeded or not.
 */
extern bool parse_float(const char *str, float *p_value);

/**
 * @brief Parse a string value.
 * @param str     String to parse.
 * @param buffer  Buffer to which to store the value.
 * @returns Whether the parsing succeeded or not.
 */
extern bool parse_string(char *str, char32_t *buffer);

/**
 * @brief Parse a boolean value.
 * @param str     String to parse.
 * @param p_value Resulting value (not modified if parsing failed).
 * @returns Whether the parsing succeeded or not.
 */
extern bool parse_bool(char *str, bool *p_value);

/**@}*/
#endif // CONFIG_CONFIG_INTERNAL_H
