/**
 * @file config_parser_lines.c
 * @brief Single line parser
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup config_import
 */

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "config/config.h"
#include "config/config_helpers.h"
#include "core/logging.h"

//! Parse one key-value pair and store it into the settings struct.
static bool do_parse_keyval(const char *key, char *value, int num, app_config *cfg);


////////////////////////////////////////////////////////////////
// Line parsing
//

bool config_parse_line(char *line, int num, app_config *cfg)
{
    // eliminate data after comment
    char *comment = strchr(line, '#');
    if (comment != NULL) {
        *comment = '\0';
    }

    // comments/empty lines -> skip
    if (strlen_without_whitespace(line) == 0) {
        return true;
    }

    char *equ = strchr(line, '=');
    if (equ == NULL) {
        log_error("invalid config syntax on line %d: missing '=' symbol", num);
        return false;
    }

    char *key_start = line;
    char *key_end   = equ;
    char *value_start = equ + 1;
    char *value_end   = value_start + strlen(value_start);

    double_trim(&key_start,   &key_end);
    double_trim(&value_start, &value_end);

    if (key_start == key_end) {
        log_error("expected nonempty key on line %d", num);
        return false;
    }

    if (value_start == value_end) {
        log_error("expected nonempty value on line %d", num);
        return false;
    }

    *key_end   = '\0';
    *value_end = '\0';
    return do_parse_keyval(key_start, value_start, num, cfg);
}


////////////////////////////////////////////////////////////////
// Key-value pair processing
//

bool do_parse_keyval(const char *key, char *value, int num, app_config *cfg)
{
    bool ok = false;
    bool found = true;

    if (keys_same(key, "correct code")) {
        ok = parse_int(value, &cfg->correct_code);

    } else if (keys_same(key, "unlocked timeout")) {
        ok = parse_int(value, &cfg->unlocked_timeout);

    } else if (keys_same(key, "encoder ticks per digit")) {
        ok = parse_int(value, &cfg->encoder_ticks_per_digit);

    } else if (keys_same(key, "motor locked pos")) {
        ok = parse_int(value, &cfg->motor_pos_locked);

    } else if (keys_same(key, "motor unlocked pos")) {
        ok = parse_int(value, &cfg->motor_pos_unlocked);

    } else if (keys_same(key, "regulator kp")) {
        ok = parse_float(value, &cfg->motor_reg_kp);

    } else if (keys_same(key, "regulator pwr min")) {
        ok = parse_float(value, &cfg->motor_reg_pwr_min);

    } else if (keys_same(key, "regulator pwr max")) {
        ok = parse_float(value, &cfg->motor_reg_pwr_max);

    } else if (keys_same(key, "text for locked")) {
        ok = parse_string(value, cfg->locked_text);

    } else if (keys_same(key, "text for unlocked")) {
        ok = parse_string(value, cfg->unlocked_text);

    } else if (keys_same(key, "text for incorrect code")) {
        ok = parse_string(value, cfg->incorrect_code_text);

    } else if (keys_same(key, "text for lock name")) {
        ok = parse_string(value, cfg->lock_name);

    } else if (keys_same(key, "drive new display")) {
        ok = parse_bool(value, &cfg->new_display);

    } else if (keys_same(key, "emulate encoders")) {
        ok = parse_bool(value, &cfg->emulate_encoders);

    } else {
        log_error("unknown key '%s' with value '%s' on line %d", key, value, num);
        found = false;
    }

    if (!ok && found) {
        log_error("couldn't parse value of key '%s' on line %d: '%s'", key, num, value);
    }

    return ok;
}
