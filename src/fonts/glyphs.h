/**
 * @file glyphs.h
 * @brief Glyph lookup utility headers
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup font_glyph
 */

#ifndef FONT_GLYPHS_H
#define FONT_GLYPHS_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <uchar.h>
#include "fonts/font_types.h"

/**
 * @addtogroup font_glyph
 * @{
 */
/**
 * @name Glyph lookup
 * @brief Wrapper functions for faster lookup & manipulation of font glyphs
 * @{
 */

//! Struct containing information about a glyph.
typedef struct {
    char32_t codepoint;      //!< UTF-32 codepoint of this glyph.
    unsigned width;          //!< Width of the glyph in pixels.
    unsigned stride;         //!< Width of the glyph in 16bit words.
    unsigned height;         //!< Height of the glyph in pixels.
    unsigned ascent;         //!< Ascent of the glyph in pixels.
    const font_bits_t *data; //!< Pixel data of the glyph.
} glyph_info;


/**
 * @brief Try to find a glyph in the specified segmented font.
 * @param codepoint Codepoint to look for.
 * @param font      Font to search in.
 * @param out       Information discovered about the glyph.
 * @returns Whether the glyph was found or not.
 */
extern bool glyph_lookup(char32_t codepoint, font_t font, glyph_info *out);

/**
 * @brief Look up the default glyph for the given font.
 * @param font  Segmented font.
 * @param out   Information discovered about the glyph.
 */
extern void glyph_lookup_default(font_t font, glyph_info *out);

/**
 * @brief Check whether a given pixel is active within the glyph grid.
 * @param x     X coordinate within the glyph.
 * @param y     Y coordinate within the glyph.
 * @param glyph Information data structure about the glyph.
 * @returns     True if the pixel is active, false otherwise.
 */
static inline bool glyph_pixel_active(unsigned x, unsigned y, const glyph_info *glyph)
{
    unsigned byte_offset = x / 16 + y * glyph->stride;
    unsigned bit_offset  = x % 16;

    return ((glyph->data[byte_offset] << bit_offset) & 0x8000) != 0;
}

/**@}*/
/**@}*/

#endif // FONT_GLYPHS_H
