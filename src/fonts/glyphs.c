/**
 * @file glyphs.c
 * @brief Glyph lookup utility implementation
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup font_glyph
 */

#include "fonts/glyphs.h"

/**
 * @addtogroup font_glyph
 * @{
 */

/**
 * @name Internal static functions
 * @{
 */
/**
 * @brief Collect information about a specific glyph (must be present in the provided segment)
 * @param codepoint  UTF-32 codepoint to collect data about.
 * @param info       Segment to collect from.
 * @param out        Output data structure containing data about the glyph.
 * @remarks Behaviour is undefined if the glyph is not present in the segment.
 */
static void collect_meta(char32_t codepoint, fontseg_t info, glyph_info *out);

/**
 * @brief Check if a codepoint is covered by this font segment.
 * @param segment    Segment to scan.
 * @param codepoint  UTF-32 codepoint to check.
 * @returns True if the codepoint was found, false otherwise.
 */
static bool has_codepoint(fontseg_t segment, char32_t codepoint);

/**@}*/

bool glyph_lookup(char32_t codepoint, font_t font, glyph_info *out)
{
    bool found;
    for (int i = 0; font[i] != NULL; i++) {
        found = has_codepoint(font[i], codepoint);

        if (found) {
            collect_meta(codepoint, font[i], out);
            break;
        }
    }

    if (!found) {
        glyph_lookup_default(font, out);
    }

    return found;
}

void glyph_lookup_default(font_t font, glyph_info *out)
{
    fontseg_t segment = font[0];
    char32_t codepoint;

    if (has_codepoint(segment, segment->defaultchar)) {
        codepoint = segment->defaultchar;
    } else {
        codepoint = segment->firstchar;
    }
    collect_meta(codepoint, segment, out);
}

void collect_meta(char32_t codepoint, fontseg_t info, glyph_info *out)
{
    uint32_t glyph_idx = codepoint - info->firstchar;
    uint32_t monospaced_unit = (info->maxwidth + 15) / 16 * info->height;
    uint32_t data_index;

    out->codepoint = codepoint;
    out->width     = info->width ? info->width[glyph_idx] : info->maxwidth;
    out->stride    = (out->width + 15) / 16;
    out->height    = info->height;
    out->ascent    = info->ascent;
    data_index     = info->offset ? info->offset[glyph_idx] : glyph_idx * monospaced_unit;
    out->data      = &info->bits[data_index];
}

bool has_codepoint(fontseg_t segment, char32_t codepoint)
{
     return (codepoint >= segment->firstchar) &&
           ((codepoint -  segment->firstchar) < segment->size);
}

/**@}*/
