/**
 * @defgroup font Font module
 * @brief Collection of fonts
 *
 * This module provides some fonts and functions to manipulate them.
 * The provided fonts are Verdana 16, Tahoma 40, a generic proportinal
 * 14x16 font and a monospace 8x16 font.
 */

/**
 * @defgroup font_struct Font structure
 * @brief Common font-related structures & declarations
 * @ingroup font
 */

/**
 * @defgroup font_bits Font definition
 * @brief Definition of included fonts
 * @ingroup font
 */

/**
 * @defgroup font_glyph Glyph utilities
 * @ingroup font
 * @brief Glyph lookup functions
 *
 * These functions provide a convenient wrapper around the font structure.
 * They allow one to lookup a glyph corresponding to a given UTF-32 codepoint.
 * A function for checking if a pixel within the glyph grid is active is also
 * provided.
 */
