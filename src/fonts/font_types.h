/**
 * @file font_types.h
 * @copyright (c) 1999, 2000, 2001, 2002, 2003, 2005, 2010, 2011 Greg Haerr <greg@censoft.com>
 *            Portions Copyright (c) 2002 by Koninklijke Philips Electronics N.V.
 * @ingroup font
 * @brief Structure of font data
 *
 * Simplified font type descriptor based on
 * Microwindows/Nano-X library by Greg Haerr
 *
 *   https://github.com/ghaerr/microwindows
 *
 * Simplification by Pavel Pisa for Czech Technical University
 * Computer Architectures course
 */

#ifndef FONT_TYPES_H
#define FONT_TYPES_H

#include <stdint.h>
#include <uchar.h>

/**
 * @addtogroup font_struct
 * @{
 */

//! Type for font bitmaps
typedef uint16_t font_bits_t;

//! Single contiguous segment of a font
typedef struct {
    char              *name;        //!< Font name
    int                maxwidth;    //!< Maximum glyph width in pixels
    unsigned int       height;      //!< Glyph height in pixels
    int                ascent;      //!< Ascent (baseline) height in pixels
    char32_t           firstchar;   //!< First UTF-32 codepoint present in this segment
    size_t             size;        //!< Segment size in codepoints
    const font_bits_t *bits;        //!< 16-bit right-padded glyph bitmap data
    const uint32_t    *offset;      //!< Per-codepoint offsets into bitmap data or 0 if fixed
    const uint8_t     *width;       //!< Per-codepoint glyph widths or 0 if fixed
    char32_t           defaultchar; //!< Default UTF-32 codepoint
    size_t             bits_size;   //!< Number of words in the bitmap data memeber
} font_descriptor_t;

//! Pointer to a single font segment
typedef font_descriptor_t *fontseg_t;

//! Font definition - NULL-terminated list of font segments
typedef fontseg_t *font_t;

/**@}*/
/**
 * @addtogroup font_bits
 * @{
 */

//! Proportional 14x16 font
extern fontseg_t font_winFreeSystem14x16[];

//! Monospace ROM 8x16 font
extern fontseg_t font_rom8x16[];

//! Verdana 16 font
extern fontseg_t font_wVerdana_16[];

//! Tahoma 40 font
extern fontseg_t font_wTahoma_40[];

/**@}*/
/**@}*/
#endif // FONT_TYPES_H
