/**
 * @file args.h
 * @brief Command line argument handler interface
 * @author Jakub Vanek
 * @author Dmitrii Zamedianskii
 * @copyright any combination of GPL, LGPL, MPL or BSD licenses
 * @ingroup args
 */

#ifndef APP_ARGS_H
#define APP_ARGS_H

/**
 * @defgroup args Commandline argument parser
 * @brief Logic of parsing command line arguments
 * @{
 */

//! Command from the argument parser
typedef enum {
    ARGS_ERROR, //!< Exit with a nonzero exit code.
    ARGS_RUN,   //!< Run the lock program.
    ARGS_EXIT   //!< Exit with a zero exit code.
} argcmd;

/**
 * @brief Parse application command line arguments.
 *
 * Based on the arguments, current global configuration is updated
 * and the decision what to do next is made.
 *
 * @param argc Argument count passed to main()
 * @param argv Argument vector passed to main()
 * @returns Action to do after this function returns
 */
extern argcmd args_process(int argc, char **argv);

/**@}*/

#endif // APP_ARGS_H
