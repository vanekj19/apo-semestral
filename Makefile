# configuration overrides
-include Makefile.conf

## various directories & files
OBJDIR     = build
DEPDIR     = $(OBJDIR)
SRCROOT    = src
EXECUTABLE = lock.elf


################################
### Default SSH config

TARGET_USER  ?= root
TARGET_IP    ?= 127.0.0.1
TARGET_DIR   ?= /tmp/locker
ifeq ($(TARGET_IP),127.0.0.1)
  TARGET_PORT ?= 2222
else
  TARGET_PORT ?= 22
endif

SSH_OPTIONS  ?= -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"
USER_CONFIG  ?= lock.cfg.example
JUMP_HOST    ?= postel.felk.cvut.cz
ifneq ($(filter tunnel,$(MAKECMDGOALS)),)
  ifeq ($(TUNNEL_IP),)
    $(error TUNNEL_IP= not set. Please set it in Makefile.conf or on command line)
  endif
  ifeq ($(CTU_USERNAME),)
    $(error CTU_USERNAME= not set. Please set it in Makefile.conf or on command line)
  endif
  ifeq ($(CTU_USERNAME),prijmeni123)
    $(error CTU_USERNAME= not set. Please set it in Makefile.conf or on command line)
  endif
endif


################################
### Compiler flags

## used c compiler
CROSS ?= arm-linux-gnueabihf-
CC     = $(CROSS)gcc

## configurable flags
OPTIMIZATION ?= -O2 -s
SANITIZERS   ?=
WARNINGS     ?= -Wall -pedantic -Werror
TARGET       ?= -mcpu=cortex-a9
STATIC       ?=

## construct other flags
# depflags - automatic generation of Make dependencies between .c and .h files
DEPFLAGS = -MMD -MP -MT $@ -MF $(patsubst $(OBJDIR)/%.o,$(DEPDIR)/%.d,$@)
# cppflags - C preprocessor flags
CPPFLAGS = -I$(SRCROOT) -D_GNU_SOURCE
# cflags - C compiler options (code generation related)
CFLAGS   = -std=c11 -pthread $(OPTIMIZATION) $(SANITIZERS) $(WARNINGS) $(TARGET)
# ldflags - linker flags, except for libraries
LDFLAGS  = $(CFLAGS) $(STATIC)
# ldlibs - libraries to link to
LDLIBS   = -lrt -lm

# user config
USER_CONFIG     ?= lock.cfg.example
USER_CONFIG_REM  = $(TARGET_DIR)/$(USER_CONFIG)

################################
### Source files

## file list
SOURCES = \
	$(SRCROOT)/main.c \
	$(SRCROOT)/args.c \
	$(SRCROOT)/logic/init_deinit.c \
	$(SRCROOT)/logic/main_flow.c \
	$(SRCROOT)/logic/ui_update.c \
	$(SRCROOT)/core/common.c \
	$(SRCROOT)/core/logging.c \
	$(SRCROOT)/core/mmio_map.c \
	$(SRCROOT)/core/mmio_regs.c \
	$(SRCROOT)/core/mmio_page.c \
	$(SRCROOT)/core/utf8.c \
	$(SRCROOT)/core/utf8_char.c \
	$(SRCROOT)/lcd/lcd.c \
	$(SRCROOT)/render/render.c \
	$(SRCROOT)/render/render_top.c \
	$(SRCROOT)/render/render_prompt.c \
	$(SRCROOT)/render/render_pin.c \
	$(SRCROOT)/render/render_bottom.c \
	$(SRCROOT)/render/drawing_font.c \
	$(SRCROOT)/render/drawing_lines.c \
	$(SRCROOT)/render/drawing_rects.c \
	$(SRCROOT)/render/drawing_img.c \
	$(SRCROOT)/fonts/glyphs.c \
	$(SRCROOT)/fonts/font_rom8x16.c \
	$(SRCROOT)/fonts/font_prop14x16.c \
	$(SRCROOT)/fonts/font_wVerdana_16.c \
	$(SRCROOT)/fonts/font_wTahoma_40.c \
	$(SRCROOT)/leds/leds.c \
	$(SRCROOT)/encoders/encoders.c \
	$(SRCROOT)/encoders/encoder_hw.c \
	$(SRCROOT)/encoders/encoder_stdin.c \
	$(SRCROOT)/encoders/utils.c \
	$(SRCROOT)/encoders/termios.c \
	$(SRCROOT)/motors/motors.c \
	$(SRCROOT)/motors/regulator.c \
	$(SRCROOT)/config/config_default.c \
	$(SRCROOT)/config/config_helpers.c \
	$(SRCROOT)/config/config_parser.c \
	$(SRCROOT)/config/config_parser_lines.c \
	$(SRCROOT)/config/config_save.c \
	$(SRCROOT)/config/config_validate.c \
	# end of list

# generate output file list
DEPFILES = $(patsubst $(SRCROOT)/%.c,$(DEPDIR)/%.d,$(SOURCES))
OBJFILES = $(patsubst $(SRCROOT)/%.c,$(OBJDIR)/%.o,$(SOURCES))


################################
### Build process

all: $(EXECUTABLE)

# compile source files to object files
$(OBJDIR)/%.o: $(SRCROOT)/%.c $(DEPDIR)/%.d | $(OBJDIR)
	@echo " [CC] $@"
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) $(CPPFLAGS) $(DEPFLAGS) -o $@ -c $<

# link object files to final executable
$(EXECUTABLE): $(OBJFILES)
	@echo " [LD] $@"
	@$(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS)

# dependency file handling; inspired by http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
$(DEPFILES):

$(OBJDIR):
	@mkdir -p $@

-include $(DEPFILES)

clean:
	rm -f $(EXECUTABLE)
	rm -rf $(OBJDIR) $(DEPDIR)

rebuild: clean all


################################
### Doxygen docs build

doxy: docs/Doxyfile $(SRCS)
	@echo " [DOXY] $<"
	@mkdir -p build
	@doxygen $<


################################
### Remote SSH utils

tunnel:
	@echo "-- Opening SSH tunnel to kit at $(TUNNEL_IP), press Ctrl+C to exit"
	ssh -nNT $(CTU_USERNAME)@$(JUMP_HOST) -L $(TARGET_PORT):$(TUNNEL_IP):22

shell:
	@echo "-- Connecting to $(TARGET_IP):$(TARGET_PORT)"
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) $(TARGET_USER)@$(TARGET_IP)

upload: $(EXECUTABLE)
	@echo "-- Uploading executable to $(TARGET_IP):$(TARGET_PORT)"
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) $(TARGET_USER)@$(TARGET_IP) mkdir -p $(TARGET_DIR)
	scp $(SSH_OPTIONS) -P $(TARGET_PORT) $(EXECUTABLE) $(USER_CONFIG) $(TARGET_USER)@$(TARGET_IP):$(TARGET_DIR)/

run: upload
	@echo "-- Running program on remote"
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) -t $(TARGET_USER)@$(TARGET_IP) $(TARGET_DIR)/$(EXECUTABLE) -c $(USER_CONFIG_REM)

remote-install-service: $(EXECUTABLE) apo-lock.service lock.cfg.example
	@echo "-- Installing as a system service"
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) $(TARGET_USER)@$(TARGET_IP) mkdir -p /opt/lock
	scp $(SSH_OPTIONS) -P $(TARGET_PORT) $(EXECUTABLE)    $(TARGET_USER)@$(TARGET_IP):/opt/lock/lock.elf
	scp $(SSH_OPTIONS) -P $(TARGET_PORT) apo-lock.service $(TARGET_USER)@$(TARGET_IP):/etc/systemd/system/apo-lock.service
	scp $(SSH_OPTIONS) -P $(TARGET_PORT) $(USER_CONFIG)   $(TARGET_USER)@$(TARGET_IP):/etc/lock.cfg
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) $(TARGET_USER)@$(TARGET_IP) systemctl daemon-reload
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) $(TARGET_USER)@$(TARGET_IP) systemctl enable apo-lock.service


################################
### ASan/UBSan helpers

install-san:
	@echo "-- Installing sanitizer libs on remote"
	ssh $(SSH_OPTIONS) -p $(TARGET_PORT) -t $(TARGET_USER)@$(TARGET_IP) apt-get install libasan4 libubsan0


################################
### TBA remote debug


################################
### Meta

.PHONY : all clean rebuild tunnel shell upload run doxy remote-install-service install-san
