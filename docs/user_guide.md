# End-user manual

It is assumed that the application has been successfully installed as a
persistent service into the MZ_APO device into the `/opt/lock` directory.
To see how to do this, please take a look at the
[installation guide](docs/installing.md).

## Starting up

To boot the device and start the application, connect the Ethernet
cable and the 12V barrel jack into the appropriate sockets on the board.
After a while a PIN prompt should appear onscreen.

## Locking, unlocking

To unlock the lock, you will need to enter a correct PIN code.
This can be done using three colored knobs at the lower end of the board.
Rotating each of the encoders a bit will increment or decrement the associated
digit on the lock screen.

After you have entered the PIN digits you can submit the code for
verification by pressing the blue knob. If the code is correct,
the lock will open and the status light will turn green. If it is incorrect,
a warning will be displayed for a second and then you will be presented
with the PIN prompt again.

After the door is unlocked you have only a limited amount of time
until the lock is locked again. The amount of remaining time will
be indicated on the yellow LED line at the bottom of the device.
If you want to re-lock the lock before the timeout expires,
press the blue knob again.

## Configuration

You can change some aspects of how the lock behaves. Currently it is not
possible to change the configuration through the lock GUI. Instead, it is
necessary to connect to the board via SSH and edit the configuration file
manually there.

In the default service deployment the configuration file can be found in
`/etc/lock.cfg`. It can be edited using `nano` or other editor directly
on the device.

The changes will not be applied immediately. You will have to restart the
application to apply the changes. This can be done by running the following
command on the device:
```
sudo systemctl restart apo-lock.service
```

### Changing the passcode

The passcode has to be a three-digit decimal number. Its value can be
changed by modifying the `correct code` configuration entry.

### Adjusting knob sensitivity

Sensitivity of the encoders can be changed with the `encoder ticks per digit`
entry. Raising the value will make the digit transition slower and vice versa.

The option value is signed - you can provide both positive and negative
numbers. Positive values mean that digits will increase with clockwise
rotation of knobs. Negative values will make the digits increase in the opposite
(counter-clockwise) direction.

### Adjusting how long the lock stays open

You can adjust the time it takes for the lock to lock again. This is controlled
by the `unlocked timeout` option.

### Adjusting the displayed time

The lock application displays current time on the top of the screen.
This is the system time on the MZ_APO device. To change the time,
you will need to change the system time. The guide for this can be found
on Debian webpages: [wiki.debian.org/DateTime](https://wiki.debian.org/DateTime).

### Adjusting the displayed texts

There is also a location name displayed in the statusbar. To change the
text that is displayed there, change the value of the `text for lock name`
option.

Similarly, you can change the other displayed texts.

### Adjusting motor regulator parameters

To adjust the response of the lock motor controller, you will need to
change the `motor *` and `regulator *` options in the configuration file.
Please see the comments in the example configuration file for more details.
