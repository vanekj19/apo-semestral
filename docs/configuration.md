# Configuration reference

## Command-line arguments

There are a few supported parameters that can be passed on the command line.

- `-h`, `--help` will display a short greeting message and a description of accepted flags.
- `-c <FILE`>, `--config <FILE>` will cause the program to load a configuration file.
  The format is described in the following section; an example configuration file can also be found in `lock.cfg.example`.

  The application can be invoked like this:

```bash
/path/to/lock.elf -c /path/to/lock.cfg
```

- `-d`, `--debug` will enable printing of debug messages when they're disabled. Whether they're enabled or disabled by default is determined by build-time configuration; see following sections for details.

## Configuration file format

The compiled application can be reconfigured using a simple text-based configuration file.
An example configuration file with extra comments can be found in [lock.cfg.example](lock.cfg.example).

The format is basically a `key = value` set with support for ignoring
comments after the `#` character. Four value types are supported:

 - strings: can be quoted or unquoted; the quotes will be stripped.
 - integers in decimal notation; they can have an optional comment-like suffix.
 - floats in decimal notation; they can have a suffix too.
 - booleans: yes/true/on/1 enable the option, no/false/off/0 disable it.

Basic FAQ-style walktrough through the available options can be found in the
[user guide](docs/user_guide.md).

An example configuration file follows:
```cfg
#############################
## Lock configuration file ##
#############################

# format:
# key = value [extra data]
# [extra data] is *ignored* if value is a number.


## UI-related configuration

# Code for unlocking the code (range 0-999)
correct code = 123

# How long will the lock stay unlocked
unlocked timeout = 32000 ms

# How much do the encoders have to turn for the displayed digit to change
encoder ticks per digit = 5 tick


## Hardware configuration

# whether a new MZ_APO display is connected (ILI9481 is newer, HX8357-C is older)
drive new display = no

# whether encoder input should be emulated using raw stdin input
emulate encoders = no


## UI strings: (you can use diacritics as long as the fonts support them)

# Status string for unlocked state
text for unlocked       = "Door is open"

# Status string for locked state
text for locked         = "Door is locked"

# Status string for incorrect code warning
text for incorrect code = "Incorrect code"

# Lock name (displayed in the top-left corner)
text for lock name      = "MZ_APO Lock"


## Motor control related configuration

# Locked & unlocked positions of the motor
# - these are dependent on the exact type of the actuator mechanism
# range: 32bit signed integer bounds
motor locked pos   =   0 ticks
motor unlocked pos = 256 ticks

# P-regulator proportional constant
# range: unbounded; reasonable values are on the order of 0.001
regulator kp      = 0.001

# Deadband compensation power applied to the motor
# beware: this is almost always applied to the motor!
# range: 0.0 - 1.0
regulator pwr min = 0.03

# Maximum applied power to the motor.
# range: 0.0 - 1.0
regulator pwr max = 0.13
```

## Build-time configuration

For more advanced customization, it is possible to adjust build-time configuration.

Application-side configuration can be found in [build_config.h](@ref build_config.h).

Compiler flags used for building the application can be customized too.
To do this, override them in `Makefile.conf`:

```makefile
# What optimizations will be made by the compiler (-Ox) and whether debug
# symbols will be generated (-gLEVEL to enable or -s to disable)
OPTIMIZATION ?= -O2 -s

# GCC/Clang sanitizers to enable (tested: -fsanitize=address  fsanitize=undefined)
SANITIZERS   ?=

# Enabled compiler warnings
WARNINGS     ?= -Wall -pedantic -Werror

# Target CPU for which the code will be compiled (MZ_APO uses dualcore ARM Cortex A9)
TARGET       ?= -mcpu=cortex-a9

# Whether static linking will be enabled (empty for no, -static for yes)
STATIC       ?=
```

## Remote knob mode

It is possible to emulate access to the lock knobs. To activate this feature, enable
the `emulate encoders` option in the configuration file. This will enable a
console-based TUI on stdin/stdout that allows the user to interact with the lock remotely.

The controls are the following:

- `a` or `z` will increment or decrement the first digit (order 100)
- `s` or `x` will increment or decrement the second digit (order 10)
- `d` or `c` will increment or decrement the third digit (order 1)
- `f` will simulate a persistent press/push-down of the blue knob (not released until `v` is pressed)
- `v` will simulate a release of the blue knob
- `i` will display current encoder module state
- `h` will display a help message similar to this one
