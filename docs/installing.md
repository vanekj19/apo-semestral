# Installation guide

## Building the application

### Prerequisites

The application can currently only be built on GNU/Linux-compatible systems.

You first need to install a GNU C11 compatible compiler for ARM platforms.
A verified way of doing this is installing the `crossbuild-essential-armhf`
meta-package on Ubuntu 18.04. This will install the necessary `arm-linux-gnueabihf-gcc`
compiler executable and accompanying libraries.

You will also need GNU `make` and OpenSSH; please install these using your
preferred software source.

On Ubuntu, the required packages can be installed with the following command:
```bash
sudo apt install crossbuild-essential-armhf make ssh
```

### Building

To build the application with a compiler installed in the system, simply run:

```bash
make rebuild
```

This should take care of everything and it will output a `lock.elf` application executable.
This file can now be uploaded to the MZ_APO device.

You can also alternatively use a musl-libc based cross compiler. This way you
can build a statically-linked standalone application without any dependencies.
The cross-compiler can be downloaded from [http://musl.cc/](http://musl.cc/);
the `armv7l-linux-musleabihf-cross` variant is known to be working.

To perform the build, run the following command:

```bash
make rebuild CROSS=<musl toolchain root>/bin/armv7l-linux-musleabihf- STATIC=-static
```

The `CROSS=` option tells the Makefile to use this prefix when looking for `gcc`.
The `STATIC=` option is an flag for the linker to use only static versions of libraries when linking.

## Uploading

This can be done via SSH. This step can either be done manually or it can be
automated via Makefile. To activate SSH support in Makefile, copy `Makefile.conf.example`
to `Makefile.conf` in the repository root and customize it for your use.

Makefile currently supports two modes of deployment - direct and tunnelled.
Direct mode is useful if you have the device in your LAN; tunnelled mode
is needed when the device is behind a SSH jump host.

Tunnelled mode is the default one. In this mode a connection has to be
established to a jump host (`postel` FEE CTU server in this case) and
a connection to MZ_APO is them made through that tunnel. On the other hand,
direct mode skips this and connects directly to the target board.

To use the direct mode, you'll need to change the following entries in `Makefile.conf`:

```makefile
# Remote SSH port (usually 22)
TARGET_PORT = 2222

# Remote SSH user
TARGET_USER = root

# Remote SSH address (direct connect: <ip>, port forwarded: 127.0.0.1)
TARGET_IP   = xx.xx.xx.xx

# Directory for uploading programs
TARGET_DIR  = /opt/lock

# Config file to upload to MZ_APO and to run the program with
USER_CONFIG = lock.cfg.example

## SSH connection options
# ignore host key changes
SSH_OPTIONS  = -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"
# set path to mzapo-root-key
SSH_OPTIONS += -i /home/kuba/.ssh/mzapo-root-key
```

If you want to use the tunnelled mode instead, you'll need to tweak these options instead:

```makefile
# Directory for uploading programs
TARGET_DIR  = /opt/lock

# Config file to upload to MZ_APO and to run the program with
USER_CONFIG = lock.cfg.example

## Remote SSH connection options
# ignore host key changes
SSH_OPTIONS  = -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"
# set path to mzapo-root-key
SSH_OPTIONS += -i /home/kuba/.ssh/mzapo

# SSH jump host
JUMP_HOST    = postel.felk.cvut.cz

# Username on jump host used for MZ_APO SSH tunnelling
CTU_USERNAME = vanekj19

# Internal IP behind the jump host to tunnel to
TUNNEL_IP    = 192.168.202.207
```

When using the tunnelled mode, you first need to establish the tunnel.
This can be done by running:

```bash
make tunnel
```

Afterwards, you can upload the program to the target directory using

```bash
make upload  # upload only, or
make run     # upload & run
```

## Customizing configuration

Sometimes you'll need to change basic configuration before the application
before the deployment. This can be done by editing `lock.cfg.example`
(or as given by `USER_CONFIG`). This configuration file will then be uploaded
to the device alongside with the executable. This file is also automatically
passed to the application when the `make run` target is used.

For details about the configuration options, please see [configuration.md](docs/configuration.md).
However, the `drive new display` option is particularly important.
You will need to set it to `yes` or `no` depending on what type of
display your MZ_APO kit has.

## Running the application

To run the lock on the remote, simply run `/path/to/lock.elf` from a SSH connection.
To stop the application, press Ctrl+C; this will relock the lock and shut down the application.

For more details about this, please see the [user guide](docs/user_guide.md).

## Installation as a system service

The application can be installed as a system service when systemd is used as the init system.
For this use case, `apo-lock.service` is provided as an example of how the application can be integrated.

To install the service unit file, copy `apo-lock.service` to `/etc/systemd/system/apo-lock.service`.
You will then need to install the lock binary as `/opt/lock/lock.elf` and provide a configuration file
in `/etc/lock.cfg`. Afterwards, the service can be set to start on boot and started:

```bash
sudo systemctl daemon-reload
sudo systemctl enable apo-lock.service
sudo systemctl start apo-lock.service
```

These steps have been also included as a special make target that will handle everything for you:

```bash
make remote-install-service
```
