Introduction
============

Welcome to the developer documentation of the MZ_APO lock application.

Below you can find a block diagram of the application:
![Application block diagram](block-diagram.png)
